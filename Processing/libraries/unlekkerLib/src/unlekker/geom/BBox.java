
<!-- saved from url=(0122)https://raw.githubusercontent.com/martymcguire/ThingiverseCollage/master/libraries/unlekkerLib/src/unlekker/geom/BBox.java -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;"> package unlekker.geom;


/** 
 * Bounding box for calculating centroids and dimensions of point data. 
 * @author &lt;a href="http://workshop.evolutionzone.com/"&gt;Marius Watz&lt;/a&gt;
*/

public class BBox {
	public Vec3 min,max,center,dim;
	
	public BBox() {
		min=new Vec3(10000,10000,10000);
		max=new Vec3(-10000,-10000,-10000);
		center=new Vec3();		
		dim=new Vec3();
	}

	/**
	 * Reset bounding box for new calculation.
	 *
	 */
	public void reset() {
		min.set(10000,10000,10000);
		max.set(-10000,-10000,-10000);
		center.set(0,0,0);
	}
	
	/**
	 * Check new point against current bounding box.
	 *
	 */
	public void addPoint(float _x,float _y,float _z) {
		if(_x&lt;min.x) min.x=_x;
		if(_y&lt;min.y) min.y=_y;
		if(_z&lt;min.z) min.z=_z;
		if(_x&gt;max.x) max.x=_x;
		if(_y&gt;max.y) max.y=_y;
		if(_z&gt;max.z) max.z=_z;
	}
	
	public float getDim() {
		dim.set(max);
		dim.sub(min);
				
  	float maxdim=dim.x;
  	if(dim.y&gt;maxdim) maxdim=dim.y;
  	if(dim.z&gt;maxdim) maxdim=dim.z;
  	
  	return maxdim;
	}

  /////////////////////////////////////////////
  // TRANSFORMS

	public void translate(float tx,float ty,float tz) {
		min.add(tx,ty,tz);
		max.add(tx,ty,tz);
		center.add(tx,ty,tz);
	}

	public void scale(float tx,float ty,float tz) {
		min.mult(tx,ty,tz);
		max.mult(tx,ty,tz);
		center.mult(tx,ty,tz);
	}
	
	public String toString() {
		String s="BBox: "+min+" - "+max+" Center: "+center;
		return s;
	}
}
</pre></body></html>