final float FINAL_fPi = 3.14159265358979323846; //<>// //<>// //<>//
//poop
ArrayList<CCelestialBody> arrayListSolarSystem;
CCelestialBody celestialBodySun;
CCelestialBody celestialBodyMercury;
CCelestialBody celestialBodyVenus;
CCelestialBody celestialBodyEarth;
CCelestialBody celestialBodyEarthMoon;
CCelestialBody celestialBodyMars;
CCelestialBody celestialBodyMarsDeimos;
CCelestialBody celestialBodyMarsPhobos;
CCelestialBody celestialBodyJupiter;
CCelestialBody celestialBodyJupiterIo;
CCelestialBody celestialBodyJupiterEuropa;
CCelestialBody celestialBodyJupiterGanymede;
CCelestialBody celestialBodyJupiterCallisto;



float fCameraEyePositionX =   0.00;
float fCameraEyePositionY = -500.00;
float fCameraEyePositionZ = 2800.00;
float fCameraDestinationX =   0.00;
float fCameraDestinationY =   0.00;
float fCameraDestinationZ =   0.00;
float fCameraUpVectorX    =   0.00;
float fCameraUpVectorY    =   1.00;
float fCameraUpVectorZ    =   0.00;

PShader shaderBumped;
PShader shaderTextured;



void setup()
{
  size(800,800,P3D);
  noStroke();
  noLights();
  
  shaderTextured = loadShader( "texfrag.glsl", "texvert.glsl" );
                                
  shaderBumped = loadShader( "bumpfrag_2.glsl", "bumpvert_2.glsl" );

  CColor colorSun  = new CColor( 255, 255, 127, 127 );
  
  float[] arrf4MaterialAmbient = { 0.10, 0.10, 0.05, 1.00 };
  
  PImage imgSun    = loadImage("sun.png");
  celestialBodySun = new CCelestialBodyTextured(    0.000 ,
                                                    0.000,
                                                  400.000,
                                                    0.000,
                                                    0.000,
                                                    0.005,
                                                  colorSun,
                                                  imgSun,
                                                  shaderTextured                   );
  celestialBodySun.Setup();
  
                                         
  CColor colorMercury = new CColor( 90, 10, 10, 256 );
  celestialBodyMercury = new CCelestialBody( 0.020 ,
                                             0.000,
                                             20.000,
                                           800.000,
                                             0.000,
                                             0.900,
                                             colorMercury   );
                                             
  CColor colorVenus = new CColor( 10, 100, 14, 256 );  
  celestialBodyVenus = new CCelestialBody( 0.015 ,
                                             0.000,
                                             32.000,
                                           1100.000,
                                             0.000,
                                             1.800,
                                             colorVenus   );
 
  
  CColor colorEarth = new CColor( 10, 20, 110, 256 );
  celestialBodyEarth = new CCelestialBody( 0.010 ,
                                           0.000,
                                           32.000,
                                         1400.000,
                                           0.000,
                                           0.900,
                                           colorEarth  );
                                           
  CColor colorEarthMoon = new CColor( 100, 100, 100, 256 );                                         
  celestialBodyEarthMoon = new CCelestialBody( 0.05 ,
                                               0.000,
                                               8.000,
                                              45.000,
                                               0.000,
                                               0.900,
                                               colorEarthMoon  );
                                               

  CColor colorMars = new CColor( 180, 50, 10, 256 );                                         
  celestialBodyMars = new CCelestialBody( 0.005 ,
                                          0.000,
                                          15.0,
                                         1700.000,
                                          0.000,
                                          0.900,
                                          colorMars  );
                                          
                                          
                                        
  celestialBodyMarsDeimos = new CCelestialBody( 0.15 ,
                                                0.000,
                                                6.00,
                                               30.000,
                                                0.000,
                                                0.900,
                                                colorEarthMoon  );
                                               
                                        
  celestialBodyMarsPhobos = new CCelestialBody( 0.10 ,
                                                0.000,
                                                7.00,
                                               50.000,
                                                0.000,
                                                0.900,
                                                colorEarthMoon  );
                                               
  CColor colorJupiter = new CColor( 80, 120, 60, 256 ); 
  PImage imgJupiter = loadImage("jupiter.jpg");
  PImage imgNormalMap = loadImage("normalmap.jpg");
  
  float[] arrf4MaterialJupiterDiffuse  = { 0.55625, 0.48437, 0.227187, 1.00000  };
  float[] arrf4MaterialJupiterSpecular = { 0.15000, 0.15000, 0.150000, 1.00000  };
  float   fMaterialShininess = 1.00;

  CCelestialBodyTextured(    0.001,
                             0.000,
                           100.000,
                          2100.000,
                             0.000,
                             0.001,
                          colorJupiter,
                          imgTexture,
 
/*  
  celestialBodyJupiter = new CCelestialBodyBumped(   0.001,
                                                     0.000,
                                                   100.000,
                                                  2100.000,
                                                     0.000,
                                                     0.001,
                                                  colorJupiter,
                                                  imgJupiter,
                                                  imgNormalMap,
                                                  arrf4MaterialAmbient,
                                                  arrf4MaterialJupiterDiffuse,
                                                  arrf4MaterialJupiterSpecular,
                                                  fMaterialShininess,                                                  
                                                  shaderBumped );
*/
  celestialBodyJupiter.Setup();
  
  celestialBodyJupiterEuropa = new CCelestialBody(  0.050,
                                                    0.000,
                                                    6.000,
                                                  135.000,
                                                    0.000,
                                                    0.900,
                                                  colorEarthMoon );
                                               
                                        
  celestialBodyJupiterIo = new CCelestialBody(  0.030,
                                                0.000,
                                                6.200,
                                              155.000,
                                                0.000,
                                                0.900,
                                               colorEarthMoon  );
                                           
  celestialBodyJupiterGanymede = new CCelestialBody( 0.0200,
                                                     0.0000,
                                                     8.0000,
                                                    170.000,
                                                     0.0000,
                                                     0.9000,
                                                     colorEarthMoon  );
                                               
                                        
  celestialBodyJupiterCallisto = new CCelestialBody( 0.015 ,
                                                     0.000 ,
                                                     6.000 ,
                                                    200.000 ,
                                                     0.000 ,
                                                     0.900 ,
                                                     colorEarthMoon  );

 /*
  celestialBodySun.SatelliteAdd( celestialBodyMercury );                                     
  celestialBodySun.SatelliteAdd( celestialBodyVenus );
  celestialBodySun.SatelliteAdd( celestialBodyEarth );
  celestialBodyEarth.SatelliteAdd( celestialBodyEarthMoon );
  celestialBodySun.SatelliteAdd( celestialBodyMars );
  celestialBodyMars.SatelliteAdd( celestialBodyMarsDeimos );
  celestialBodyMars.SatelliteAdd( celestialBodyMarsPhobos );*/
  celestialBodySun.SatelliteAdd( celestialBodyJupiter );
  /*
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterEuropa );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterIo );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterGanymede );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterCallisto ); */
}

void draw()
{
  camera( fCameraEyePositionX,
          fCameraEyePositionY,
          fCameraEyePositionZ,
          fCameraDestinationX,
          fCameraDestinationY,
          fCameraDestinationZ,
          fCameraUpVectorX,
          fCameraUpVectorY,
          fCameraUpVectorZ     );
  
  background(0);
  perspective();
  

  celestialBodySun.Print();
  celestialBodySun.Draw();
  

}
