#define PROCESSING_TEXTURE_SHADER

varying vec3 v3LightDirection;
varying vec3 v3EyeDirection;
varying vec4 v4TexCoord0;

uniform mat4 transform; // gl_ModelViewMatrix
uniform mat4 texMatrix;
uniform mat3 normalMatrix; // gl_NormalMatrix
uniform vec3 v3LightPosition;


attribute vec4 vertex;
attribute vec3 normal;
attribute vec3 tangent;
attribute vec2 texCoord;


void main()
{

	gl_Position = transform * vertex; //ftransform();
	v4TexCoord0 = texMatrix * vec4( texCoord, 1.0, 1.0 );
	
		
	vec3 v3TangentComponent =
		normalized( normalMatrix * tangent );	
	
	vec3 v3BumpComponent =
		cross( v3NormalComponent,
			   v3TangentComponent );
	
	vec3 v3NormalComponent =
		normalize( normalMatrix * normal );			   
	
	vec3 v3LightDirectionRaw;
	v3LightDirectionRaw.x = dot( v3LightPosition, v3TangentComponent );
	v3LightDirectionRaw.y = dot( v3LightPosition, v3BumpComponent    );
	v3LightDirectionRaw.z = dot( v3LightPosition, v3NormalComponent  );
	v3LightDirection = normalize( v3LightDirectionRaw );
	
	vec3 v3EyeDirectionRaw;
	v3EyeDirection = vec3( transform * vertex );
	v3EyeDirectionRaw.x = dot( v3EyeDirection, v3TangentComponent );
	v3EyeDirectionRaw.y = dot( v3EyeDirection, v3BumpComponent    );
	v3EyeDirectionRaw.z = dot( v3EyeDirection, v3NormalComponent  );
	v3EyeDirection = normalize( v3EyeDirectionRaw );
	
}