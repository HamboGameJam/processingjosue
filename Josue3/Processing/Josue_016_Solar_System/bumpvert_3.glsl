#define PROCESSING_TEXTURE_SHADER

struct tData
{
	vec3 v3Normal;
	vec4 v4Position;	
	vec4 v4TexCoord;
};

uniform mat4 transform;
uniform mat4 texMatrix;
uniform mat3 normalMatrix;

attribute vec4 vertex;
attribute vec3 normal;
attribute vec3 tangent;
attribute vec2 texCoord;

varying tData data;

void main()
{
	data.v3Normal = normalize( normalMatrix * normal );
	data.v4Position = transform * vertex;
	data.v4TexCoord = texMatrix * vec4( texCoord, 1.0, 1.0 );
	
	gl_Position = transform * vertex;
}
