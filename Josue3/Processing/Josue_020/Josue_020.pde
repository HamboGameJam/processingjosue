final float FINAL_fPi = 3.14159265358979323846;

ArrayList<CCelestialBody> arrayListSolarSystem;
CCelestialBody celestialBodySun;
CCelestialBody celestialBodyMercury;
CCelestialBody celestialBodyVenus;
CCelestialBody celestialBodyEarth;
CCelestialBody celestialBodyEarthMoon;
CCelestialBody celestialBodyMars;
CCelestialBody celestialBodyMarsDeimos;
CCelestialBody celestialBodyMarsPhobos;
CCelestialBody celestialBodyJupiter;
CCelestialBody celestialBodyJupiterIo;
CCelestialBody celestialBodyJupiterEuropa;
CCelestialBody celestialBodyJupiterGanymede;
CCelestialBody celestialBodyJupiterCallisto;
CCelestialBody celestialBodySaturn;
CCelestialBody celestialBodyUranus;
CCelestialBody celestialBodyNeptune;
CCelestialBody celestialBodyPluto;

float fCameraEyePositionX =    0.00;
float fCameraEyePositionY = -500.00;
float fCameraEyePositionZ = 2800.00;
float fCameraDestinationX =    0.00;
float fCameraDestinationY =    0.00;
float fCameraDestinationZ =    0.00;
float fCameraUpVectorX    =    0.00;
float fCameraUpVectorY    =    1.00;
float fCameraUpVectorZ    =    0.00;

PShader shaderColored;
PShader shaderPointLit;
PShader shaderTextured;


void setup()
{
  size(1600,800,P3D);
  noStroke();
  noLights();
  
  shaderColored  = loadShader( "ColoredFrag.glsl", "ColoredVert.glsl" );
  shaderTextured = loadShader( "TexturedFrag.glsl", "TexturedVert.glsl" );
  shaderPointLit = loadShader( "PointLitFrag.glsl", "PointLitVert.glsl" );

  CColor colorSun  = new CColor( 255, 255, 127, 127 );
  
  float[] arrf4MaterialAmbient = { 0.10, 0.10, 0.05, 1.00 };
  
  PImage imgSun    = loadImage("sun.png");
  celestialBodySun = new CCelestialBodyTextured(    0.000,
                                                    0.000,
                                                  400.000,
                                                    0.000,
                                                    0.000,
                                                    0.005,
                                                 colorSun,
                                                   imgSun,
                                           shaderTextured  );
  celestialBodySun.Setup();
  
                                         
  CColor colorMercury = new CColor( 90, 10, 10, 256 );
  PImage imgMercury    = loadImage("mercury.png");
  celestialBodyMercury = new CCelestialBodySpherePointLit(   0.020,
                                                             0.000,
                                                            20.000,
                                                           800.000,
                                                             0.000,
                                                             0.900,
                                                      colorMercury,
                                                        imgMercury,
                                                    shaderPointLit  );
  celestialBodyMercury.Setup();
  
  CColor colorVenus = new CColor( 10, 100, 14, 256 );
  PImage imgVenus    = loadImage("venus.png");  
  celestialBodyVenus = new CCelestialBodySpherePointLit(    0.015,
                                                            0.000,
                                                           32.000,
                                                         1100.000,
                                                            0.000,
                                                            1.800,
                                                       colorVenus,
                                                         imgVenus,
                                                   shaderPointLit  );
  celestialBodyVenus.Setup();
  
  CColor colorEarth = new CColor( 255, 255, 255, 255 );
  PImage imgEarth    = loadImage("earth.jpg");  
  celestialBodyEarth = new CCelestialBodySpherePointLit(    0.010,
                                                            0.000,
                                                           52.000,
                                                         1400.000,
                                                            0.000,
                                                            0.010,
                                                       colorEarth,
                                                         imgEarth,
                                                   shaderPointLit  );
  celestialBodyEarth.Setup();
                                           
  CColor colorEarthMoon = new CColor( 100, 100, 100, 256 );
  PImage imgMoon    = loadImage("moon.jpg");
  celestialBodyEarthMoon = new CCelestialBodySpherePointLit(     0.050,
                                                                 0.000,
                                                                17.500,
                                                                85.000,
                                                                 0.000,
                                                                 0.050,
                                                        colorEarthMoon,
                                                               imgMoon,
                                                        shaderPointLit  );
  celestialBodyEarthMoon.Setup();                                               

  CColor colorMars = new CColor( 180, 180, 180, 256 );
  PImage imgMars = loadImage("mars.png");
  celestialBodyMars = new CCelestialBodySpherePointLit(   0.005,
                                                          0.000,
                                                         45.000,
                                                       1700.000,
                                                          0.000,
                                                          0.001,
                                                      colorMars,
                                                        imgMars,
                                                 shaderPointLit  );
  celestialBodyMars.Setup();                                         
                                          
  CColor colorMarsDeimos = new CColor( 180, 160, 10, 256 );                                      
  celestialBodyMarsDeimos = new CCelestialBodySpherePointLit( 0.150,
                                                              0.000,
                                                             14.000,
                                                             80.000,
                                                              0.000,
                                                              0.001,
                                                    colorMarsDeimos,
                                                            imgMoon,
                                                     shaderPointLit  );
  celestialBodyMarsDeimos.Setup();                                             
  
  CColor colorMarsPhobos = new CColor( 200, 180, 100, 256 );                                      
  celestialBodyMarsPhobos = new CCelestialBodySpherePointLit(  0.100,
                                                               0.000,
                                                              18.000,
                                                             150.000,
                                                               0.000,
                                                               0.0015,
                                                     colorMarsPhobos,
                                                             imgMoon,
                                                      shaderPointLit  );
  celestialBodyMarsPhobos.Setup();
  
  CColor colorJupiter = new CColor( 80, 120, 60, 256 ); 
  PImage imgJupiter = loadImage("jupiter.jpg"); 
  celestialBodyJupiter = new CCelestialBodySpherePointLit(   0.001,
                                                                            0.000,
                                                                         100.000,
                                                                        2100.000,
                                                                            0.000,
                                                                            0.001,
                                                                    colorJupiter,
                                                                      imgJupiter,                                                  
                                                                  shaderPointLit );

  celestialBodyJupiter.Setup();
  
  CColor colorJupiterEuropa = new CColor( 120, 80, 150, 256 ); 
  celestialBodyJupiterEuropa = new CCelestialBodySpherePointLit(  0.050,
                                                                  0.000,
                                                                  8.000,
                                                                135.000,
                                                                  0.000,
                                                                  0.090,
                                                     colorJupiterEuropa,
                                                             imgJupiter,
                                                         shaderPointLit );
  celestialBodyJupiterEuropa.Setup();                                             
                                        
  CColor colorJupiterIo = new CColor( 55, 180, 70, 256 );                                        
  celestialBodyJupiterIo = new CCelestialBodySpherePointLit(  0.030,
                                                              0.000,
                                                              9.200,
                                                            155.000,
                                                              0.000,
                                                              0.100,
                                                     colorJupiterIo,
                                                            imgMoon,
                                                     shaderPointLit );
  celestialBodyJupiterIo.Setup();                                             

  CColor colorJupiterGanymede = new CColor( 185, 180, 20, 256 );                                                                     
  celestialBodyJupiterGanymede = new CCelestialBodySpherePointLit( 0.0200,
                                                                   0.0000,
                                                                   8.0000,
                                                                  170.000,
                                                                   0.0000,
                                                                   0.0700,
                                                     colorJupiterGanymede,
                                                                  imgMoon,
                                                           shaderPointLit );
  celestialBodyJupiterGanymede.Setup();
  
  CColor colorJupiterCallisto = new CColor( 185, 30, 20, 256 );                                      
  celestialBodyJupiterCallisto = new CCelestialBodySpherePointLit(  0.015,
                                                                    0.000,
                                                                   10.000,
                                                                  200.000,
                                                                    0.000,
                                                                    0.050,
                                                     colorJupiterCallisto,
                                                                  imgMoon,
                                                           shaderPointLit );
  celestialBodyJupiterCallisto.Setup();
  
  CColor colorSaturn = new CColor( 155, 130, 20, 256 );
  PImage imgSaturn = loadImage("saturn.png");
  celestialBodySaturn = new CCelestialBodyRingedSpherePointLit(     0.0001,
                                                                    0.000,
                                                                   90.000,
                                                                 2500.000,
                                                                    0.000,
                                                                    0.003,
                                                              colorSaturn,
                                                                imgSaturn,
                                                           shaderPointLit,
                                                            shaderColored  );                                                           
  celestialBodySaturn.Setup();


  
  CRing ringSaturn0 = new CRing( 135.00,
                                 115.00,
                                 new CColor( 91, 75, 49, 128 ),
                                 new CColor( 49, 35, 26, 168 ) );
                              
  CRing ringSaturn1 = new CRing( 150.00,
                                 135.00,
                                 ( new CColor( 49, 35, 26, 168 ) ),
                                 ( new CColor( 91, 75, 49, 128 ) )  );
                                 
  CRing ringSaturn2 = new CRing( 180.00,
                                 150.00,
                                 ( new CColor( 91, 75, 49, 128 ) ),
                                 ( new CColor( 49, 35, 26, 128 ) )  );
                                 
  CRing ringSaturn3 = new CRing( 220.00,
                                 180.00,
                                 ( new CColor( 91, 75, 49, 128 ) ),
                                 ( new CColor( 237, 209, 156, 184 ) )  );
                                 
  CRing ringSaturn4 = new CRing( 260.00,
                                 230.00,
                                 ( new CColor(  91,  75,  49, 184 ) ),
                                 ( new CColor( 237, 209, 156, 96 ) )  );                                 
  /*   */                                 
  ((CCelestialBodyRingedSpherePointLit)celestialBodySaturn).RingAdd( ringSaturn0 );
  ((CCelestialBodyRingedSpherePointLit)celestialBodySaturn).RingAdd( ringSaturn1 );
  ((CCelestialBodyRingedSpherePointLit)celestialBodySaturn).RingAdd( ringSaturn2 );
  ((CCelestialBodyRingedSpherePointLit)celestialBodySaturn).RingAdd( ringSaturn3 );
  ((CCelestialBodyRingedSpherePointLit)celestialBodySaturn).RingAdd( ringSaturn4 );
  
  CColor colorUranus = new CColor( 30, 30, 200, 256 );
  PImage imgUranus = loadImage("uranus.png");
  celestialBodyUranus = new CCelestialBodyRingedSpherePointLit(     0.0005,
                                                                    0.0001,
                                                                   60.0000,
                                                                 3000.0000,
                                                                    0.0000,
                                                                    0.0011,
                                                               colorUranus,
                                                                 imgUranus,
                                                            shaderPointLit,
                                                             shaderColored  );
  celestialBodyUranus.Setup();

  CColor colorNeptune = new CColor( 10, 180, 180, 256 );
  PImage imgNeptune = loadImage("neptune.jpg");
  celestialBodyNeptune = new CCelestialBodySpherePointLit(    0.00025,
                                                              0.00050,
                                                             45.00000,
                                                           3300.00000,
                                                              0.00000,
                                                               0.0005,
                                                         colorNeptune,
                                                           imgNeptune,
                                                       shaderPointLit );
  celestialBodyNeptune.Setup();
 
  CColor colorPluto = new CColor( 70, 70, 70, 256 );
  PImage imgPluto = loadImage("pluto.png");
  celestialBodyPluto = new CCelestialBodySpherePointLit(     0.000005,
                                                                    0.0001,
                                                                   33.000,
                                                                 3700.000,
                                                                    0.000,
                                                                    0.0003,
                                                               colorPluto,
                                                                 imgPluto,
                                                           shaderPointLit );
  celestialBodyPluto.Setup();  
  /*
  CCelestialBody celestialBodyUranus;
CCelestialBody celestialBodyNeptune;
CCelestialBody celestialBodyPluto;
  */
  
  celestialBodySun.SatelliteAdd( celestialBodyMercury );                                    
  celestialBodySun.SatelliteAdd( celestialBodyVenus ); 
  celestialBodySun.SatelliteAdd( celestialBodyEarth );
  celestialBodyEarth.SatelliteAdd( celestialBodyEarthMoon );
  celestialBodySun.SatelliteAdd( celestialBodyMars );
  celestialBodyMars.SatelliteAdd( celestialBodyMarsDeimos );
  celestialBodyMars.SatelliteAdd( celestialBodyMarsPhobos );
  celestialBodySun.SatelliteAdd( celestialBodyJupiter );
  
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterEuropa );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterIo );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterGanymede );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterCallisto );
  
  celestialBodySun.SatelliteAdd( celestialBodySaturn );
  celestialBodySun.SatelliteAdd( celestialBodyUranus );
  celestialBodySun.SatelliteAdd( celestialBodyNeptune );
  celestialBodySun.SatelliteAdd( celestialBodyPluto ); 
}

void draw()
{
  camera( fCameraEyePositionX,
          fCameraEyePositionY,
          fCameraEyePositionZ,
          fCameraDestinationX,
          fCameraDestinationY,
          fCameraDestinationZ,
          fCameraUpVectorX,
          fCameraUpVectorY,
          fCameraUpVectorZ     );
  

  background(0);
  perspective();
  
  pointLight( 256, 256, 256, 000, 000, 000 );  

  celestialBodySun.Print();
  celestialBodySun.Draw();
  

}