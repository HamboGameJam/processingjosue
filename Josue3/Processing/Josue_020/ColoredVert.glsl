#define PROCESSING_LIGHT_SHADER

uniform mat4 transform;
uniform vec4 lightPosition;

attribute vec4 vertex;
attribute vec4 color;

varying vec4 vertColor;

void main()
{
  gl_Position = transform * vertex;
    
  vertColor = color;//v4Color;
 
}