#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;

varying vec4 vertColor;
varying vec4 vertTexCoord;

void main()
{
  vec4 v4Sum = vec4(0);
  
  int j, i;
  
  for( int i = -4; i < 4; i++ )
  {
    for( int j = -3; j < 3; j++ )
	{
	  v4Sum += texture2D( texture, vertTexCoord.st + vec2(j,i) * 0.004) * 0.25;
	}
  }
  
  if(  texture2D( texture, vertTexCoord.st).r < 0.3  )
  {
    gl_FragColor = v4Sum * v4Sum * 0.012 + texture2D(texture, vertTexCoord.st);
  }
  else
  {
	  if(  texture2D( texture, vertTexCoord.st).r < 0.5  )
	  {
        gl_FragColor = v4Sum * v4Sum * 0.009 + texture2D(texture, vertTexCoord.st);	  
	  }
	  else
	  {
	    gl_FragColor = v4Sum * v4Sum * 0.0075 + texture2D(texture, vertTexCoord.st);	  
	  }
  }
  
  //gl_FragColor = texture2D(texture, vertTexCoord.st) * vertColor;
}