//#define PROCESSING_TEXLIGHT_SHADER
  #define PROCESSING_TEXLIGHT_SHADER
//#define PROCESSING_LIGHT_SHADER

uniform mat4 transform;
uniform mat3 normalMatrix;
uniform mat4 texMatrix;
uniform vec4 lightPosition;

attribute vec4 vertex;
attribute vec2 texCoord;

varying vec4 vertTexCoord;

void main()
{
  //mat3 inverseMatrix = inverse(normalMatrix);
  //inverseMatrix * 
  gl_Position = transform * vertex;
  vertTexCoord = texMatrix * vec4(texCoord, 1.0, 1.0);
}