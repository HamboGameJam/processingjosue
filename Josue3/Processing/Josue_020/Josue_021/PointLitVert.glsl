#define PROCESSING_TEXLIGHT_SHADER

uniform mat4 modelview;
uniform mat4 transform;
uniform mat4 texMatrix;
uniform mat3 normalMatrix;
uniform vec4 lightPosition;

uniform vec4 v4Color;
//uniform vec4 v4LightPosition;

attribute vec4 vertex;
attribute vec3 normal;
attribute vec2 texCoord;

varying vec4 vertColor;
varying vec3 ecNormal;
varying vec3 lightDir;
varying vec4 vertTexCoord;

//varying vec3 v3PositionEye;



void main()
{ 
  gl_Position = transform * vertex;
  vec3 ecVertex = vec3( modelview * vertex );
  ecNormal = normalize( normalMatrix * normal );
  lightDir = normalize( lightPosition.xyz - ecVertex );
  
  /*
  vec3 v3PositionEye = (gl_ModelViewMatrix * vertex).xyz;
  lightDir = normalize(( gl_LightSource[0].position ).xyz - v3PositionEye);
  */
  
  vertColor = v4Color;
  vertTexCoord = texMatrix * vec4( texCoord, 1.0, 1.0 );
  
}