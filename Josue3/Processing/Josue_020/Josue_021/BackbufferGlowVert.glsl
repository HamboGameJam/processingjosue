#define PROCESSING_LIGHT_SHADER

uniform mat4 transform;
uniform vec4 lightPosition;

attribute vec4 vertex;


void main()
{
  gl_Position = transform * vertex;
    

}