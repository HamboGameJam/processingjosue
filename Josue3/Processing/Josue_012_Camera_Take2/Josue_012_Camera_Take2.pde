
/*
The camera() function is really hard to use and debug. If you really want control over
your scene, you might use the PeasyCam library by Jonathan Feinberg. This library can
be downloaded at http://mrfeinberg.com/peasycam/. Another option is the
Obsessive Camera Direction (OCD) library by Kristian Linn Damkjer, available for
download at http://gdsstudios.com/processing/libraries/ocd/.
*/
import processing.opengl.*;
import damkjer.ocd.*;


float x;
float z;

Camera objOCDCameraMain = new Camera(this, 0, 0, -100);
void cylinder( int numSegments, float h, float r )
{
  float angle = 360.0 / (float)numSegments;
  
  // top
  beginShape();
  for ( int i = 0; i < numSegments; i++ )
  {
    float x = cos( radians( angle * i ) ) * r;
    float y = sin( radians( angle * i ) ) * r;
    vertex( x, y, -h/2 );
  }
  endShape( CLOSE );
  
  // side
  beginShape( QUAD_STRIP );
  for ( int i = 0; i < numSegments + 1; i++ )
  {
    float x = cos( radians( angle * i ) ) * r;
    float y = sin( radians( angle * i ) ) * r;
    vertex( x, y, -h/2 );
    vertex( x, y, h/2 );
  }
  endShape();
  
  // bottom
  beginShape();
  for ( int i = 0; i < numSegments; i++ )
  {
    float x = cos( radians( angle * i ) ) * r;
    float y = sin( radians( angle * i ) ) * r;
    vertex( x, y, h/2 );
  }
  endShape( CLOSE );
}

void setup()
{
  size( 640, 480, OPENGL );
  x = 0;
  z = 0;
  //noStroke();
}

void draw()
{
  background( 255 );
  lights();
  //objOCDCameraMain.aim( 30, 100, 50 );
  //objOCDCameraMain.feed();
  //objOCDCameraMain.aim(10, 10, 0);
  /*
  x = cos( radians( frameCount ) ) * 1000;
  z = sin( radians( frameCount ) ) * 1000;
  camera( x, 0, z, width/2, height-50, -500, 0, 1, 0 );
*/
/*
  beginShape();  
    fill( 255, 0, 0 );
    vertex( 0, height, 0);
    fill( 255, 255, 0 );
    vertex( 0, height, -1000 );
    fill( 0, 255, 0 );
    vertex( width, height, -1000 );
    fill( 0, 0, 255 );
    vertex( width, height, 0 );
  endShape(CLOSE);
*/  
  //fill( 255 );
  
  /*
  for( int iX  = -100;
           iX >=  100;
           iX +=   10  )
  {
    for( int iY  = -100;
             iY >=  100;
             iY +=   10  )
    {
      for( int iZ  = -100;
               iZ >=  100;
               iZ +=   10  )
      {
        pushMatrix();
          translate(iX, iY, iZ );
          fill( 255, 255, 0 );
          box( 5 );
        popMatrix();        
      }    
    }    
  }
  */
  pushMatrix();
    translate( width*.3, height*.3, 0 );
    rotateY( radians( frameCount ) );
    fill( 255, 0, 0 );
    cylinder( 30, 100, 50 );
  popMatrix();
  
  pushMatrix();
    translate( width*.7, height*.5, 0 );
    rotateY( radians( frameCount ) );
    fill( 255, 255, 0 );
    cylinder( 4, 200, 50 );
  popMatrix();
  
  pushMatrix();
    translate( width*.3, height*.7, 0 );
    rotateY( radians( frameCount ) );
    fill( 0, 0, 255 );
    cylinder( 3, 200, 30 );
  popMatrix();  
  


   
}

