public class CColor
{  
  private color m_colorObject;
  
  CColor( int r, int g, int b, int a )
  {
    this.m_colorObject = color( r, g, b, a );
  }  
  
  public int Alpha()
  {
    return (m_colorObject >> 24) & 0xFF;
  }  
  
  public int Red()
  {
    return (m_colorObject >> 16) & 0xFF;
  }
    
  public int Green()
  {
    return (m_colorObject >> 8) & 0xFF;
  }
  
  public int Blue()
  {
    return m_colorObject & 0xFF;
  }
  
  public void Print()
  {
    print( "\nRed:   " + ( ( m_colorObject >> 16 ) & 0xFF ) );
    print( "\nGreen: " + ( ( m_colorObject >>  8 ) & 0xFF ) );
    print( "\nBlue:  " + ( ( m_colorObject       ) & 0xFF ) );
    print( "\nAlpha: " + ( ( m_colorObject >> 24 ) & 0xFF ) );
  }
}
