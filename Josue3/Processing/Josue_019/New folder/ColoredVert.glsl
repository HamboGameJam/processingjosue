#define PROCESSING_LIGHT_SHADER

uniform mat4 transform;
uniform vec4 v4Color;
uniform vec4 lightPosition;

attribute vec4 vertex;

varying vec4 vertColor;

void main()
{
  gl_Position = transform * vertex;
    
  vertColor = v4Color;
 
}