




<!DOCTYPE html>
<html class="   ">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    
    <title>ThingiverseCollage/libraries/unlekkerLib/src/unlekker/test/TestSTL.java at master · martymcguire/ThingiverseCollage · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-144.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144.png" />
    <meta property="fb:app_id" content="1401488693436528"/>

      <meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="martymcguire/ThingiverseCollage" name="twitter:title" /><meta content="ThingiverseCollage - Processing sketch to fill a 2D SVG shape with images of a bunch of random 3D STL objects." name="twitter:description" /><meta content="https://avatars1.githubusercontent.com/u/14772?s=400" name="twitter:image:src" />
<meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="https://avatars1.githubusercontent.com/u/14772?s=400" property="og:image" /><meta content="martymcguire/ThingiverseCollage" property="og:title" /><meta content="https://github.com/martymcguire/ThingiverseCollage" property="og:url" /><meta content="ThingiverseCollage - Processing sketch to fill a 2D SVG shape with images of a bunch of random 3D STL objects." property="og:description" />

    <link rel="assets" href="https://github.global.ssl.fastly.net/">
    <link rel="conduit-xhr" href="https://ghconduit.com:25035/">
    <link rel="xhr-socket" href="/_sockets" />

    <meta name="msapplication-TileImage" content="/windows-tile.png" />
    <meta name="msapplication-TileColor" content="#ffffff" />
    <meta name="selected-link" value="repo_source" data-pjax-transient />
    <meta content="collector.githubapp.com" name="octolytics-host" /><meta content="collector-cdn.github.com" name="octolytics-script-host" /><meta content="github" name="octolytics-app-id" /><meta content="4059C9FD:2E4E:60B554:5355BF49" name="octolytics-dimension-request_id" />
    

    
    
    <link rel="icon" type="image/x-icon" href="https://github.global.ssl.fastly.net/favicon.ico" />

    <meta content="authenticity_token" name="csrf-param" />
<meta content="LU7T4oZcHDz0yDyFCjGpV2U0OH5AJwODzxrHK+Y1uGyMtpwgY22Vp/wGbtJ4w4tIesb8MLmYsbqSHga99/XUng==" name="csrf-token" />

    <link href="https://github.global.ssl.fastly.net/assets/github-f928be83799a73f1f8eda72a1fd459915073420f.css" media="all" rel="stylesheet" type="text/css" />
    <link href="https://github.global.ssl.fastly.net/assets/github2-0a16120e50749ebd0a7bbb45d3c848b65919ec7e.css" media="all" rel="stylesheet" type="text/css" />
    


        <script crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/frameworks-f8f8d8ee1afb4365ba5e002fdbc3c8e61738713b.js" type="text/javascript"></script>
        <script async="async" crossorigin="anonymous" src="https://github.global.ssl.fastly.net/assets/github-cd7f10efb41ab8f286e0046a56ecd4950b85ee7c.js" type="text/javascript"></script>
        
        
      <meta http-equiv="x-pjax-version" content="f846cccb67e770e3ac9844ed2484f5fe">

        <link data-pjax-transient rel='permalink' href='/martymcguire/ThingiverseCollage/blob/db14f30fdb0a8fcdc31ac3bb779f1f32b7f9a269/libraries/unlekkerLib/src/unlekker/test/TestSTL.java'>

  <meta name="description" content="ThingiverseCollage - Processing sketch to fill a 2D SVG shape with images of a bunch of random 3D STL objects." />

  <meta content="14772" name="octolytics-dimension-user_id" /><meta content="martymcguire" name="octolytics-dimension-user_login" /><meta content="792361" name="octolytics-dimension-repository_id" /><meta content="martymcguire/ThingiverseCollage" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="792361" name="octolytics-dimension-repository_network_root_id" /><meta content="martymcguire/ThingiverseCollage" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/martymcguire/ThingiverseCollage/commits/master.atom" rel="alternate" title="Recent Commits to ThingiverseCollage:master" type="application/atom+xml" />

  </head>


  <body class="logged_out  env-production windows vis-public page-blob">
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>
    <div class="wrapper">
      
      
      
      


      
      <div class="header header-logged-out">
  <div class="container clearfix">

    <a class="header-logo-wordmark" href="https://github.com/">
      <span class="mega-octicon octicon-logo-github"></span>
    </a>

    <div class="header-actions">
        <a class="button primary" href="/join">Sign up</a>
      <a class="button signin" href="/login?return_to=%2Fmartymcguire%2FThingiverseCollage%2Fblob%2Fmaster%2Flibraries%2FunlekkerLib%2Fsrc%2Funlekker%2Ftest%2FTestSTL.java">Sign in</a>
    </div>

    <div class="command-bar js-command-bar  in-repository">

      <ul class="top-nav">
          <li class="explore"><a href="/explore">Explore</a></li>
        <li class="features"><a href="/features">Features</a></li>
          <li class="enterprise"><a href="https://enterprise.github.com/">Enterprise</a></li>
          <li class="blog"><a href="/blog">Blog</a></li>
      </ul>
        <form accept-charset="UTF-8" action="/search" class="command-bar-form" id="top_search_form" method="get">

<div class="commandbar">
  <span class="message"></span>
  <input type="text" data-hotkey="/ s" name="q" id="js-command-bar-field" placeholder="Search or type a command" tabindex="1" autocapitalize="off"
    
    
      data-repo="martymcguire/ThingiverseCollage"
      data-branch="master"
      data-sha="39a121ef723d1fdace04ff3eb53129dd31ac528d"
  >
  <div class="display hidden"></div>
</div>

    <input type="hidden" name="nwo" value="martymcguire/ThingiverseCollage" />

    <div class="select-menu js-menu-container js-select-menu search-context-select-menu">
      <span class="minibutton select-menu-button js-menu-target" role="button" aria-haspopup="true">
        <span class="js-select-button">This repository</span>
      </span>

      <div class="select-menu-modal-holder js-menu-content js-navigation-container" aria-hidden="true">
        <div class="select-menu-modal">

          <div class="select-menu-item js-navigation-item js-this-repository-navigation-item selected">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" class="js-search-this-repository" name="search_target" value="repository" checked="checked" />
            <div class="select-menu-item-text js-select-button-text">This repository</div>
          </div> <!-- /.select-menu-item -->

          <div class="select-menu-item js-navigation-item js-all-repositories-navigation-item">
            <span class="select-menu-item-icon octicon octicon-check"></span>
            <input type="radio" name="search_target" value="global" />
            <div class="select-menu-item-text js-select-button-text">All repositories</div>
          </div> <!-- /.select-menu-item -->

        </div>
      </div>
    </div>

  <span class="help tooltipped tooltipped-s" aria-label="Show command bar help">
    <span class="octicon octicon-question"></span>
  </span>


  <input type="hidden" name="ref" value="cmdform">

</form>
    </div>

  </div>
</div>



      <div id="start-of-content" class="accessibility-aid"></div>
          <div class="site" itemscope itemtype="http://schema.org/WebPage">
    
    <div class="pagehead repohead instapaper_ignore readability-menu">
      <div class="container">
        

<ul class="pagehead-actions">


  <li>
    <a href="/login?return_to=%2Fmartymcguire%2FThingiverseCollage"
    class="minibutton with-count js-toggler-target star-button tooltipped tooltipped-n"
    aria-label="You must be signed in to star a repository" rel="nofollow">
    <span class="octicon octicon-star"></span>Star
  </a>

    <a class="social-count js-social-count" href="/martymcguire/ThingiverseCollage/stargazers">
      6
    </a>

  </li>

    <li>
      <a href="/login?return_to=%2Fmartymcguire%2FThingiverseCollage"
        class="minibutton with-count js-toggler-target fork-button tooltipped tooltipped-n"
        aria-label="You must be signed in to fork a repository" rel="nofollow">
        <span class="octicon octicon-git-branch"></span>Fork
      </a>
      <a href="/martymcguire/ThingiverseCollage/network" class="social-count">
        1
      </a>
    </li>
</ul>

        <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title public">
          <span class="repo-label"><span>public</span></span>
          <span class="mega-octicon octicon-repo"></span>
          <span class="author"><a href="/martymcguire" class="url fn" itemprop="url" rel="author"><span itemprop="title">martymcguire</span></a></span><!--
       --><span class="path-divider">/</span><!--
       --><strong><a href="/martymcguire/ThingiverseCollage" class="js-current-repository js-repo-home-link">ThingiverseCollage</a></strong>

          <span class="page-context-loader">
            <img alt="Octocat-spinner-32" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div class="container">
      <div class="repository-with-sidebar repo-container new-discussion-timeline js-new-discussion-timeline  ">
        <div class="repository-sidebar clearfix">
            

<div class="sunken-menu vertical-right repo-nav js-repo-nav js-repository-container-pjax js-octicon-loaders">
  <div class="sunken-menu-contents">
    <ul class="sunken-menu-group">
      <li class="tooltipped tooltipped-w" aria-label="Code">
        <a href="/martymcguire/ThingiverseCollage" aria-label="Code" class="selected js-selected-navigation-item sunken-menu-item" data-gotokey="c" data-pjax="true" data-selected-links="repo_source repo_downloads repo_commits repo_tags repo_branches /martymcguire/ThingiverseCollage">
          <span class="octicon octicon-code"></span> <span class="full-word">Code</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

        <li class="tooltipped tooltipped-w" aria-label="Issues">
          <a href="/martymcguire/ThingiverseCollage/issues" aria-label="Issues" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="i" data-selected-links="repo_issues /martymcguire/ThingiverseCollage/issues">
            <span class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
            <span class='counter'>0</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>        </li>

      <li class="tooltipped tooltipped-w" aria-label="Pull Requests">
        <a href="/martymcguire/ThingiverseCollage/pulls" aria-label="Pull Requests" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-gotokey="p" data-selected-links="repo_pulls /martymcguire/ThingiverseCollage/pulls">
            <span class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
            <span class='counter'>0</span>
            <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>


    </ul>
    <div class="sunken-menu-separator"></div>
    <ul class="sunken-menu-group">

      <li class="tooltipped tooltipped-w" aria-label="Pulse">
        <a href="/martymcguire/ThingiverseCollage/pulse" aria-label="Pulse" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="pulse /martymcguire/ThingiverseCollage/pulse">
          <span class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Graphs">
        <a href="/martymcguire/ThingiverseCollage/graphs" aria-label="Graphs" class="js-selected-navigation-item sunken-menu-item" data-pjax="true" data-selected-links="repo_graphs repo_contributors /martymcguire/ThingiverseCollage/graphs">
          <span class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>

      <li class="tooltipped tooltipped-w" aria-label="Network">
        <a href="/martymcguire/ThingiverseCollage/network" aria-label="Network" class="js-selected-navigation-item sunken-menu-item js-disable-pjax" data-selected-links="repo_network /martymcguire/ThingiverseCollage/network">
          <span class="octicon octicon-git-branch"></span> <span class="full-word">Network</span>
          <img alt="Octocat-spinner-32" class="mini-loader" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32.gif" width="16" />
</a>      </li>
    </ul>


  </div>
</div>

              <div class="only-with-full-nav">
                

  

<div class="clone-url open"
  data-protocol-type="http"
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><strong>HTTPS</strong> clone URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/martymcguire/ThingiverseCollage.git" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/martymcguire/ThingiverseCollage.git" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>

  

<div class="clone-url "
  data-protocol-type="subversion"
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><strong>Subversion</strong> checkout URL</h3>
  <div class="clone-url-box">
    <input type="text" class="clone js-url-field"
           value="https://github.com/martymcguire/ThingiverseCollage" readonly="readonly">

    <span aria-label="copy to clipboard" class="js-zeroclipboard url-box-clippy minibutton zeroclipboard-button" data-clipboard-text="https://github.com/martymcguire/ThingiverseCollage" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


<p class="clone-options">You can clone with
      <a href="#" class="js-clone-selector" data-protocol="http">HTTPS</a>
      or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>.
  <span class="help tooltipped tooltipped-n" aria-label="Get help on which URL is right for you.">
    <a href="https://help.github.com/articles/which-remote-url-should-i-use">
    <span class="octicon octicon-question"></span>
    </a>
  </span>
</p>


  <a href="http://windows.github.com" class="minibutton sidebar-button" title="Save martymcguire/ThingiverseCollage to your computer and use it in GitHub Desktop." aria-label="Save martymcguire/ThingiverseCollage to your computer and use it in GitHub Desktop.">
    <span class="octicon octicon-device-desktop"></span>
    Clone in Desktop
  </a>

                <a href="/martymcguire/ThingiverseCollage/archive/master.zip"
                   class="minibutton sidebar-button"
                   aria-label="Download martymcguire/ThingiverseCollage as a zip file"
                   title="Download martymcguire/ThingiverseCollage as a zip file"
                   rel="nofollow">
                  <span class="octicon octicon-cloud-download"></span>
                  Download ZIP
                </a>
              </div>
        </div><!-- /.repository-sidebar -->

        <div id="js-repo-pjax-container" class="repository-content context-loader-container" data-pjax-container>
          


<!-- blob contrib key: blob_contributors:v21:f6dde98294f026200c1f9002c1f9b258 -->

<p title="This is a placeholder element" class="js-history-link-replace hidden"></p>

<a href="/martymcguire/ThingiverseCollage/find/master" data-pjax data-hotkey="t" class="js-show-file-finder" style="display:none">Show File Finder</a>

<div class="file-navigation">
  

<div class="select-menu js-menu-container js-select-menu" >
  <span class="minibutton select-menu-button js-menu-target" data-hotkey="w"
    data-master-branch="master"
    data-ref="master"
    role="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <span class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span class="js-select-button">master</span>
  </span>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <span class="select-menu-title">Switch branches/tags</span>
        <span class="octicon octicon-remove-close js-menu-close"></span>
      </div> <!-- /.select-menu-header -->

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" class="js-select-menu-tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div class="select-menu-item js-navigation-item selected">
              <span class="select-menu-item-icon octicon octicon-check"></span>
              <a href="/martymcguire/ThingiverseCollage/blob/master/libraries/unlekkerLib/src/unlekker/test/TestSTL.java"
                 data-name="master"
                 data-skip-pjax="true"
                 rel="nofollow"
                 class="js-navigation-open select-menu-item-text js-select-button-text css-truncate-target"
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div class="breadcrumb">
    <span class='repo-root js-repo-root'><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/martymcguire/ThingiverseCollage" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">ThingiverseCollage</span></a></span></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/martymcguire/ThingiverseCollage/tree/master/libraries" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">libraries</span></a></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/martymcguire/ThingiverseCollage/tree/master/libraries/unlekkerLib" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">unlekkerLib</span></a></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/martymcguire/ThingiverseCollage/tree/master/libraries/unlekkerLib/src" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">src</span></a></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/martymcguire/ThingiverseCollage/tree/master/libraries/unlekkerLib/src/unlekker" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">unlekker</span></a></span><span class="separator"> / </span><span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/martymcguire/ThingiverseCollage/tree/master/libraries/unlekkerLib/src/unlekker/test" data-branch="master" data-direction="back" data-pjax="true" itemscope="url"><span itemprop="title">test</span></a></span><span class="separator"> / </span><strong class="final-path">TestSTL.java</strong> <span aria-label="copy to clipboard" class="js-zeroclipboard minibutton zeroclipboard-button" data-clipboard-text="libraries/unlekkerLib/src/unlekker/test/TestSTL.java" data-copied-hint="copied!"><span class="octicon octicon-clippy"></span></span>
  </div>
</div>


  <div class="commit commit-loader file-history-tease js-deferred-content" data-url="/martymcguire/ThingiverseCollage/contributors/master/libraries/unlekkerLib/src/unlekker/test/TestSTL.java">
    Fetching contributors…

    <div class="participation">
      <p class="loader-loading"><img alt="Octocat-spinner-32-eaf2f5" height="16" src="https://github.global.ssl.fastly.net/images/spinners/octocat-spinner-32-EAF2F5.gif" width="16" /></p>
      <p class="loader-error">Cannot retrieve contributors at this time</p>
    </div>
  </div>

<div class="file-box">
  <div class="file">
    <div class="meta clearfix">
      <div class="info file-name">
        <span class="icon"><b class="octicon octicon-file-text"></b></span>
        <span class="mode" title="File Mode">executable file</span>
        <span class="meta-divider"></span>
          <span>80 lines (61 sloc)</span>
          <span class="meta-divider"></span>
        <span>1.697 kb</span>
      </div>
      <div class="actions">
        <div class="button-group">
            <a class="minibutton tooltipped tooltipped-w"
               href="http://windows.github.com" aria-label="Open this file in GitHub for Windows">
                <span class="octicon octicon-device-desktop"></span> Open
            </a>
              <a class="minibutton disabled tooltipped tooltipped-w" href="#"
                 aria-label="You must be signed in to make or propose changes">Edit</a>
          <a href="/martymcguire/ThingiverseCollage/raw/master/libraries/unlekkerLib/src/unlekker/test/TestSTL.java" class="button minibutton " id="raw-url">Raw</a>
            <a href="/martymcguire/ThingiverseCollage/blame/master/libraries/unlekkerLib/src/unlekker/test/TestSTL.java" class="button minibutton js-update-url-with-hash">Blame</a>
          <a href="/martymcguire/ThingiverseCollage/commits/master/libraries/unlekkerLib/src/unlekker/test/TestSTL.java" class="button minibutton " rel="nofollow">History</a>
        </div><!-- /.button-group -->
          <a class="minibutton danger disabled empty-icon tooltipped tooltipped-w" href="#"
             aria-label="You must be signed in to make or propose changes">
          Delete
        </a>
      </div><!-- /.actions -->
    </div>
        <div class="blob-wrapper data type-java js-blob-data">
        <table class="file-code file-diff tab-size-8">
          <tr class="file-code-line">
            <td class="blob-line-nums">
              <span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>

            </td>
            <td class="blob-line-code"><div class="code-body highlight"><pre><div class='line' id='LC1'><span class="kn">package</span> <span class="n">unlekker</span><span class="o">.</span><span class="na">test</span><span class="o">;</span></div><div class='line' id='LC2'><br/></div><div class='line' id='LC3'><span class="kn">import</span> <span class="nn">processing.core.*</span><span class="o">;</span></div><div class='line' id='LC4'><span class="kn">import</span> <span class="nn">processing.opengl.*</span><span class="o">;</span></div><div class='line' id='LC5'><span class="kn">import</span> <span class="nn">unlekker.data.*</span><span class="o">;</span></div><div class='line' id='LC6'><span class="kn">import</span> <span class="nn">unlekker.geom.*</span><span class="o">;</span></div><div class='line' id='LC7'><br/></div><div class='line' id='LC8'><br/></div><div class='line' id='LC9'><span class="kd">public</span> <span class="kd">class</span> <span class="nc">TestSTL</span> <span class="kd">extends</span> <span class="n">PApplet</span> <span class="o">{</span></div><div class='line' id='LC10'><br/></div><div class='line' id='LC11'>	<span class="n">STL</span> <span class="n">stl</span><span class="o">;</span></div><div class='line' id='LC12'>	<span class="n">FaceList</span> <span class="n">poly</span><span class="o">;</span></div><div class='line' id='LC13'><br/></div><div class='line' id='LC14'>	<span class="kd">public</span> <span class="kt">void</span> <span class="nf">setup</span><span class="o">()</span> <span class="o">{</span></div><div class='line' id='LC15'>	  <span class="n">size</span><span class="o">(</span><span class="mi">400</span><span class="o">,</span><span class="mi">400</span><span class="o">,</span> <span class="n">P3D</span><span class="o">);</span></div><div class='line' id='LC16'>	  <span class="n">frameRate</span><span class="o">(</span><span class="mi">25</span><span class="o">);</span></div><div class='line' id='LC17'>	  <span class="n">sphereDetail</span><span class="o">(</span><span class="mi">12</span><span class="o">);</span></div><div class='line' id='LC18'>	<span class="o">}</span></div><div class='line' id='LC19'><br/></div><div class='line' id='LC20'>	<span class="kd">public</span> <span class="kt">void</span> <span class="nf">draw</span><span class="o">()</span> <span class="o">{</span></div><div class='line' id='LC21'>	  <span class="n">translate</span><span class="o">(</span><span class="n">width</span><span class="o">/</span><span class="mi">2</span><span class="o">,</span><span class="n">height</span><span class="o">/</span><span class="mi">2</span><span class="o">);</span></div><div class='line' id='LC22'><br/></div><div class='line' id='LC23'>	  <span class="c1">// First write the STL data</span></div><div class='line' id='LC24'>	  <span class="k">if</span><span class="o">(</span><span class="n">frameCount</span><span class="o">==</span><span class="mi">10</span><span class="o">)</span> <span class="n">outputSTL</span><span class="o">();</span></div><div class='line' id='LC25'>	  <span class="c1">// Then read it back in</span></div><div class='line' id='LC26'>	  <span class="k">else</span> <span class="nf">if</span><span class="o">(</span><span class="n">frameCount</span><span class="o">==</span><span class="mi">11</span><span class="o">)</span> <span class="n">readSTL</span><span class="o">();</span></div><div class='line' id='LC27'><br/></div><div class='line' id='LC28'>	  <span class="c1">// If data has been read, then draw it</span></div><div class='line' id='LC29'>	  <span class="k">if</span><span class="o">(</span><span class="n">poly</span><span class="o">!=</span><span class="kc">null</span><span class="o">)</span> <span class="o">{</span></div><div class='line' id='LC30'>	    <span class="n">background</span><span class="o">(</span><span class="mi">0</span><span class="o">);</span></div><div class='line' id='LC31'>	    <span class="n">noStroke</span><span class="o">();</span></div><div class='line' id='LC32'>	    <span class="n">lights</span><span class="o">();</span></div><div class='line' id='LC33'>	    <span class="n">rotateY</span><span class="o">(</span><span class="n">radians</span><span class="o">(</span><span class="n">frameCount</span><span class="o">));</span></div><div class='line' id='LC34'>	    <span class="n">rotateX</span><span class="o">(</span><span class="n">radians</span><span class="o">(</span><span class="n">frameCount</span><span class="o">*</span><span class="mf">0.25f</span><span class="o">));</span></div><div class='line' id='LC35'>	    <span class="n">fill</span><span class="o">(</span><span class="mi">0</span><span class="o">,</span><span class="mi">200</span><span class="o">,</span><span class="mi">255</span><span class="o">,</span> <span class="mi">128</span><span class="o">);</span></div><div class='line' id='LC36'><br/></div><div class='line' id='LC37'>	    <span class="n">poly</span><span class="o">.</span><span class="na">draw</span><span class="o">(</span><span class="k">this</span><span class="o">);</span></div><div class='line' id='LC38'>	  <span class="o">}</span></div><div class='line' id='LC39'>	<span class="o">}</span></div><div class='line' id='LC40'><br/></div><div class='line' id='LC41'>	<span class="kd">public</span> <span class="kt">void</span> <span class="nf">readSTL</span><span class="o">()</span> <span class="o">{</span></div><div class='line' id='LC42'>	  <span class="c1">// Read STL file</span></div><div class='line' id='LC43'>	  <span class="n">stl</span><span class="o">=</span><span class="k">new</span> <span class="n">STL</span><span class="o">(</span><span class="k">this</span><span class="o">,</span><span class="s">&quot;Boxes.stl&quot;</span><span class="o">);</span></div><div class='line' id='LC44'><br/></div><div class='line' id='LC45'>	  <span class="c1">// Get polygon data</span></div><div class='line' id='LC46'>	  <span class="n">poly</span><span class="o">=</span><span class="n">stl</span><span class="o">.</span><span class="na">getPolyData</span><span class="o">();</span></div><div class='line' id='LC47'><br/></div><div class='line' id='LC48'>	  <span class="n">poly</span><span class="o">.</span><span class="na">normalize</span><span class="o">(</span><span class="mi">400</span><span class="o">);</span> <span class="c1">// normalize object to 400 radius</span></div><div class='line' id='LC49'>	  <span class="n">println</span><span class="o">(</span><span class="n">poly</span><span class="o">.</span><span class="na">bb</span><span class="o">.</span><span class="na">toString</span><span class="o">());</span></div><div class='line' id='LC50'>	  <span class="n">poly</span><span class="o">.</span><span class="na">center</span><span class="o">();</span> <span class="c1">// center it around world origin</span></div><div class='line' id='LC51'>	<span class="o">}</span></div><div class='line' id='LC52'><br/></div><div class='line' id='LC53'>	<span class="kd">public</span> <span class="kt">void</span> <span class="nf">outputSTL</span><span class="o">()</span> <span class="o">{</span></div><div class='line' id='LC54'>	  <span class="kt">float</span> <span class="n">rad</span><span class="o">;</span></div><div class='line' id='LC55'><br/></div><div class='line' id='LC56'>	  <span class="c1">// Initialize STL output</span></div><div class='line' id='LC57'>	  <span class="n">stl</span><span class="o">=(</span><span class="n">STL</span><span class="o">)</span><span class="n">beginRaw</span><span class="o">(</span><span class="s">&quot;unlekker.data.STL&quot;</span><span class="o">,</span><span class="s">&quot;Boxes.stl&quot;</span><span class="o">);</span></div><div class='line' id='LC58'><br/></div><div class='line' id='LC59'>	  <span class="c1">// Draw random shapes</span></div><div class='line' id='LC60'>	  <span class="k">for</span><span class="o">(</span><span class="kt">int</span> <span class="n">i</span><span class="o">=</span><span class="mi">0</span><span class="o">;</span> <span class="n">i</span><span class="o">&lt;</span><span class="mi">200</span><span class="o">;</span> <span class="n">i</span><span class="o">++)</span> <span class="o">{</span></div><div class='line' id='LC61'>	    <span class="n">pushMatrix</span><span class="o">();</span></div><div class='line' id='LC62'>	    <span class="n">translate</span><span class="o">(</span><span class="n">random</span><span class="o">(-</span><span class="mi">200</span><span class="o">,</span><span class="mi">200</span><span class="o">),</span><span class="mi">0</span><span class="o">,-</span><span class="n">random</span><span class="o">(</span><span class="mi">400</span><span class="o">));</span></div><div class='line' id='LC63'>	    <span class="n">rotateX</span><span class="o">(((</span><span class="kt">float</span><span class="o">)(</span><span class="kt">int</span><span class="o">)</span><span class="n">random</span><span class="o">(</span><span class="mi">6</span><span class="o">))*</span><span class="n">radians</span><span class="o">(</span><span class="mi">30</span><span class="o">));</span></div><div class='line' id='LC64'>	    <span class="n">rotateY</span><span class="o">(((</span><span class="kt">float</span><span class="o">)(</span><span class="kt">int</span><span class="o">)</span><span class="n">random</span><span class="o">(</span><span class="mi">6</span><span class="o">))*</span><span class="n">radians</span><span class="o">(</span><span class="mi">30</span><span class="o">));</span></div><div class='line' id='LC65'><br/></div><div class='line' id='LC66'>	    <span class="n">rad</span><span class="o">=</span><span class="n">random</span><span class="o">(</span><span class="mi">5</span><span class="o">,</span><span class="mi">25</span><span class="o">);</span></div><div class='line' id='LC67'>	    <span class="k">if</span><span class="o">(</span><span class="n">random</span><span class="o">(</span><span class="mi">100</span><span class="o">)&gt;</span><span class="mi">5</span><span class="o">)</span> <span class="n">box</span><span class="o">(</span><span class="n">rad</span><span class="o">,</span><span class="n">random</span><span class="o">(</span><span class="mi">50</span><span class="o">,</span><span class="mi">200</span><span class="o">),</span><span class="n">rad</span><span class="o">);</span></div><div class='line' id='LC68'>	    <span class="k">else</span> <span class="nf">sphere</span><span class="o">(</span><span class="n">rad</span><span class="o">);</span></div><div class='line' id='LC69'>	    <span class="n">popMatrix</span><span class="o">();</span></div><div class='line' id='LC70'>	  <span class="o">}</span></div><div class='line' id='LC71'><br/></div><div class='line' id='LC72'>	  <span class="c1">// End STL output</span></div><div class='line' id='LC73'>	  <span class="n">endRaw</span><span class="o">();</span></div><div class='line' id='LC74'>	<span class="o">}</span>	</div><div class='line' id='LC75'>	<span class="kd">public</span> <span class="kd">static</span> <span class="kt">void</span> <span class="nf">main</span><span class="o">(</span><span class="n">String</span> <span class="o">[]</span> <span class="n">args</span><span class="o">)</span> <span class="o">{</span></div><div class='line' id='LC76'>		<span class="n">PApplet</span><span class="o">.</span><span class="na">main</span><span class="o">(</span><span class="k">new</span> <span class="n">String</span> <span class="o">[]</span> <span class="o">{</span><span class="s">&quot;unlekker.test.TestSTL&quot;</span><span class="o">});</span></div><div class='line' id='LC77'>	<span class="o">}</span></div><div class='line' id='LC78'><br/></div><div class='line' id='LC79'><span class="o">}</span></div></pre></div></td>
          </tr>
        </table>
  </div>

  </div>
</div>

<a href="#jump-to-line" rel="facebox[.linejump]" data-hotkey="l" class="js-jump-to-line" style="display:none">Jump to Line</a>
<div id="jump-to-line" style="display:none">
  <form accept-charset="UTF-8" class="js-jump-to-line-form">
    <input class="linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" autofocus>
    <button type="submit" class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div class="container">
  <div class="site-footer">
    <ul class="site-footer-links right">
      <li><a href="https://status.github.com/">Status</a></li>
      <li><a href="http://developer.github.com">API</a></li>
      <li><a href="http://training.github.com">Training</a></li>
      <li><a href="http://shop.github.com">Shop</a></li>
      <li><a href="/blog">Blog</a></li>
      <li><a href="/about">About</a></li>

    </ul>

    <a href="/">
      <span class="mega-octicon octicon-mark-github" title="GitHub"></span>
    </a>

    <ul class="site-footer-links">
      <li>&copy; 2014 <span title="0.02879s from github-fe127-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="/site/terms">Terms</a></li>
        <li><a href="/site/privacy">Privacy</a></li>
        <li><a href="/security">Security</a></li>
        <li><a href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div class="fullscreen-overlay js-fullscreen-overlay" id="fullscreen_overlay">
  <div class="fullscreen-container js-fullscreen-container">
    <div class="textarea-wrap">
      <textarea name="fullscreen-contents" id="fullscreen-contents" class="fullscreen-contents js-fullscreen-contents" placeholder="" data-suggester="fullscreen_suggester"></textarea>
    </div>
  </div>
  <div class="fullscreen-sidebar">
    <a href="#" class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w" aria-label="Exit Zen Mode">
      <span class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a href="#" class="theme-switcher js-theme-switcher tooltipped tooltipped-w"
      aria-label="Switch themes">
      <span class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div id="ajax-error-message" class="flash flash-error">
      <span class="octicon octicon-alert"></span>
      <a href="#" class="octicon octicon-remove-close close js-ajax-error-dismiss"></a>
      Something went wrong with that request. Please try again.
    </div>

  </body>
</html>

