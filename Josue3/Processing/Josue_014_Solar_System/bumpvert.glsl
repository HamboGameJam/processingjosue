#define PROCESSING_TEXTURE_SHADER

uniform mat4 transform;
uniform mat4 texMatrix;
uniform vec4 v4Color;

attribute vec4 inVertex;
//attribute vec4 inColor;
attribute vec2 inTexcoord;

varying vec4 vertColor;
varying vec4 vertTexCoord;

void main() {
  gl_Position = transform * inVertex;
    
  vertColor = v4Color;
  vertTexCoord = texMatrix * vec4(inTexcoord , 1.0, 1.0);
}