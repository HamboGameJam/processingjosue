//import processing.opengl.*; //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>// //<>//


final float FINAL_fPi = 3.14159265358979323846;

ArrayList<CCelestialBody> arrayListSolarSystem;
CCelestialBody celestialBodySun;
CCelestialBody celestialBodyMercury;
CCelestialBody celestialBodyVenus;
CCelestialBody celestialBodyEarth;
CCelestialBody celestialBodyEarthMoon;
CCelestialBody celestialBodyMars;
CCelestialBody celestialBodyMarsDeimos;
CCelestialBody celestialBodyMarsPhobos;
CCelestialBody celestialBodyJupiter;
CCelestialBody celestialBodyJupiterIo;
CCelestialBody celestialBodyJupiterEuropa;
CCelestialBody celestialBodyJupiterGanymede;
CCelestialBody celestialBodyJupiterCallisto;



float fCameraEyePositionX =   0.00;
float fCameraEyePositionY = -500.00;
float fCameraEyePositionZ = 2800.00;
float fCameraDestinationX =   0.00;
float fCameraDestinationY =   0.00;
float fCameraDestinationZ =   0.00;
float fCameraUpVectorX    =   0.00;
float fCameraUpVectorY    =   1.00;
float fCameraUpVectorZ    =   0.00;

PShader shaderBumped;
PShader shaderTextured;



void setup()
{
  size(800,800,P3D);
  lights();
  noStroke();
  ambientLight( 255, 255, 255 );
  
  shaderTextured = loadShader(  "texvert.glsl",  
                                "texfrag.glsl"  );
                                
  shaderBumped = loadShader(  "bumpvert.glsl", 
                              "bumpfrag.glsl"  );

  CColor colorSun  = new CColor( 255, 255, 127, 127 );
  PImage imgSun    = loadImage("sun.png");
  celestialBodySun = new CCelestialBodyTextured(    0.000 /*fOrbitalSpeed*/,
                                                    0.000 /*fOrbitalRotation*/,
                                                  400.000 /*fRadius*/,
                                                    0.000 /*fSatelliteDistance*/,
                                                    0.000 /*fYRotation*/,
                                                    0.090 /*fYRotationalSpeed*/,
                                                  colorSun,
                                                  imgSun,
                                                  shaderTextured                   );
  celestialBodySun.Setup();
  
                                         
  CColor colorMercury = new CColor( 90, 10, 10, 256 );
  celestialBodyMercury = new CCelestialBody( 0.020 /*fOrbitalSpeed*/,
                                             0.000 /*fOrbitalRotation*/,
                                             20.000 /*fRadius*/,
                                           800.000 /*fSatelliteDistance*/,
                                             0.000 /*fYRotation*/,
                                             0.900 /*fYRotationalSpeed*/,
                                             colorMercury   );
                                             
  CColor colorVenus = new CColor( 10, 100, 14, 256 );  
  celestialBodyVenus = new CCelestialBody( 0.015 /*fOrbitalSpeed*/,
                                             0.000 /*fOrbitalRotation*/,
                                             32.000 /*fRadius*/,
                                           1100.000 /*fSatelliteDistance*/,
                                             0.000 /*fYRotation*/,
                                             1.800 /*fYRotationalSpeed*/,
                                             colorVenus   );
 
  
  CColor colorEarth = new CColor( 10, 20, 110, 256 );
  celestialBodyEarth = new CCelestialBody( 0.010 /*fOrbitalSpeed*/,
                                           0.000 /*fOrbitalRotation*/,
                                           32.000 /*fRadius*/,
                                         1400.000 /*fSatelliteDistance*/,
                                           0.000 /*fYRotation*/,
                                           0.900 /*fYRotationalSpeed*/,
                                           colorEarth                     );
                                           
  CColor colorEarthMoon = new CColor( 100, 100, 100, 256 );                                         
  celestialBodyEarthMoon = new CCelestialBody( 0.05 /*fOrbitalSpeed*/,
                                               0.000 /*fOrbitalRotation*/,
                                               8.000 /*fRadius*/,
                                              45.000 /*fSatelliteDistance*/,
                                               0.000 /*fYRotation*/,
                                               0.900 /*fYRotationalSpeed*/,
                                               colorEarthMoon                     );
                                               

  CColor colorMars = new CColor( 180, 50, 10, 256 );                                         
  celestialBodyMars = new CCelestialBody( 0.005 /*fOrbitalSpeed*/,
                                          0.000 /*fOrbitalRotation*/,
                                          15.0 /*fRadius*/,
                                         1700.000 /*fSatelliteDistance*/,
                                          0.000 /*fYRotation*/,
                                          0.900 /*fYRotationalSpeed*/,
                                          colorMars                     );
                                          
                                          
                                        
  celestialBodyMarsDeimos = new CCelestialBody( 0.15 /*fOrbitalSpeed*/,
                                                0.000 /*fOrbitalRotation*/,
                                                6.00 /*fRadius*/,
                                               30.000 /*fSatelliteDistance*/,
                                                0.000 /*fYRotation*/,
                                                0.900 /*fYRotationalSpeed*/,
                                                colorEarthMoon                     );
                                               
                                        
  celestialBodyMarsPhobos = new CCelestialBody( 0.10 /*fOrbitalSpeed*/,
                                                0.000 /*fOrbitalRotation*/,
                                                7.00 /*fRadius*/,
                                               50.000 /*fSatelliteDistance*/,
                                                0.000 /*fYRotation*/,
                                                0.900 /*fYRotationalSpeed*/,
                                                colorEarthMoon                     );
                                               
  CColor colorJupiter = new CColor( 40, 60, 30, 256 ); 
  PImage imgJupiter = loadImage("jupiter.png");
  PImage imgNormalMap = loadImage("normalmap.jpg");
  celestialBodyJupiter = new CCelestialBodyBumped(   0.001,
                                                     0.000,
                                                   100.00,
                                                  2100.000,
                                                     0.000,
                                                     0.900,
                                                  colorJupiter,
                                                  imgJupiter,
                                                  imgNormalMap,
                                                  shaderBumped );
 /*
                        PImage  imgTexture,
                        PImage  imgNormalMap,
                        PShader shaderBumped          
 */
                                           
  celestialBodyJupiterEuropa = new CCelestialBody( 0.05 ,
                                                   0.000  ,
                                                   6.00  ,
                                                  135.000  ,
                                                   0.000  ,
                                                   0.900  ,
                                                   colorEarthMoon                     );
                                               
                                        
  celestialBodyJupiterIo = new CCelestialBody( 0.03  ,
                                               0.000  ,
                                               6.200  ,
                                              155.000  ,
                                               0.000  ,
                                               0.900  ,
                                               colorEarthMoon                     );
                                           
  celestialBodyJupiterGanymede = new CCelestialBody( 0.02  ,
                                                     0.000  ,
                                                     8.00  ,
                                                    170.000  ,
                                                     0.000  ,
                                                     0.900  ,
                                                     colorEarthMoon                     );
                                               
                                        
  celestialBodyJupiterCallisto = new CCelestialBody( 0.015 ,
                                                     0.000 ,
                                                     6.000 ,
                                                    200.000 ,
                                                     0.000 ,
                                                     0.900 ,
                                                     colorEarthMoon                     );  
  /**/                                             
  /**/

    
  
  /*
  celestialBodySun.SatelliteAdd( celestialBodyMercury );                                     
  celestialBodySun.SatelliteAdd( celestialBodyVenus );
  celestialBodySun.SatelliteAdd( celestialBodyEarth );
  celestialBodyEarth.SatelliteAdd( celestialBodyEarthMoon );
  celestialBodySun.SatelliteAdd( celestialBodyMars );
  celestialBodyMars.SatelliteAdd( celestialBodyMarsDeimos );
  celestialBodyMars.SatelliteAdd( celestialBodyMarsPhobos );
  celestialBodySun.SatelliteAdd( celestialBodyJupiter );
  
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterEuropa );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterIo );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterGanymede );
  celestialBodyJupiter.SatelliteAdd( celestialBodyJupiterCallisto );*/
}

void draw()
{
  camera( fCameraEyePositionX,
          fCameraEyePositionY,
          fCameraEyePositionZ,
          fCameraDestinationX,
          fCameraDestinationY,
          fCameraDestinationZ,
          fCameraUpVectorX,
          fCameraUpVectorY,
          fCameraUpVectorZ     );
  
  background(0);
  perspective();
  

  celestialBodySun.Print();
  celestialBodySun.Draw();
  

}
