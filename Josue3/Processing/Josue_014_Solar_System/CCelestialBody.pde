//import processing.opengl.*; //<>// //<>// //<>// //<>// //<>//


public class CCelestialBody
{
  public float     m_fOrbitalSpeed;
  public float     m_fOrbitalRotation;
  public float     m_fRadius;
  public float     m_fSatelliteDistance;
  public float     m_fYAxisRotation;
  public float     m_fYAxisRotationalSpeed;
  public CColor    m_colorSurface;
  public ArrayList<CCelestialBody> m_arrayListSatellites;
  //tilt
  
  CCelestialBody( float     fOrbitalSpeed,
                  float     fOrbitalRotation,
                  float     fRadius,
                  float     fSatelliteDistance,
                  float     fYAxisRotation,
                  float     fYAxisRotationalSpeed,
                  CColor    colorSurface           )
  {
    this.m_fOrbitalSpeed = fOrbitalSpeed;
    this.m_fOrbitalRotation = fOrbitalRotation;
    this.m_fRadius = fRadius;
    this.m_fSatelliteDistance = fSatelliteDistance;
    this.m_fYAxisRotation = fYAxisRotation;    
    this.m_fYAxisRotationalSpeed = fYAxisRotationalSpeed;
    this.m_colorSurface = colorSurface;
    this.m_arrayListSatellites = new ArrayList<CCelestialBody>();
    
    
    
  }
  
  public void Print()
  {
    print( "\nOrbital Speed:"      + this.m_fOrbitalSpeed );
    print( "\nOrbital Rotation:"   + this.m_fOrbitalRotation );
    print( "\nRadius:"             + this.m_fRadius );
    print( "\nSatellite Distance:" + this.m_fSatelliteDistance );
    print( "\nY Axis Rotation:"    + this.m_fYAxisRotation );    
    print( "\nY Axis Rotational Speed:"    + this.m_fYAxisRotationalSpeed );   
    //this.m_colorSurface.Print();
  }
  
  public void Setup()
  {

  }
  
  public void SatelliteAdd( CCelestialBody celestialBodySatellite )
  {
    m_arrayListSatellites.add( celestialBodySatellite );
  }
  
  public void SatelliteRemove( CCelestialBody celestialBodySatellite )
  {
    m_arrayListSatellites.remove( celestialBodySatellite );
  }
  
  protected void Draw_Render()
  {
    fill(  this.m_colorSurface.Red(),
           this.m_colorSurface.Green(),
           this.m_colorSurface.Blue()   );
        
    sphere(m_fRadius);  
  }
  
  protected void Draw_RotateTranslate()
  {
    this.m_fOrbitalRotation = ( this.m_fOrbitalSpeed * frameCount );
    this.m_fYAxisRotation   = ( this.m_fYAxisRotationalSpeed * frameCount );

    pushMatrix();
      rotateY( this.m_fOrbitalRotation );
      translate( 0, 0, this.m_fSatelliteDistance );
      rotateY(-this.m_fOrbitalRotation );
      pushMatrix();        
        rotateY( this.m_fYAxisRotation );
  }
  
  protected void Draw_After()
  {
    /**/
        for( int iSatelliteIndex = 0;
                 iSatelliteIndex < this.m_arrayListSatellites.size();
                 iSatelliteIndex ++                                   )
        {
          ( this.m_arrayListSatellites.get(iSatelliteIndex) ).Draw();
          
        }        

      popMatrix();
      
      
    popMatrix();
  }
  
  public void Draw( )
  {
    //shader(null);
    Draw_RotateTranslate();
    Draw_Render();
    Draw_After();
  }

}
public class CCelestialBodyBumped extends CCelestialBody
{
  public PImage  m_imgTexture;
  public PImage  m_imgNormalMap; 
  public PShader m_shaderBumped;
  public PShape  m_shapeSphere;
  
  CCelestialBodyBumped( float   fOrbitalSpeed,
                        float   fOrbitalRotation,
                        float   fRadius,
                        float   fSatelliteDistance,
                        float   fYAxisRotation,
                        float   fYAxisRotationalSpeed,
                        CColor  colorSurface,
                        PImage  imgTexture,
                        PImage  imgNormalMap,
                        PShader shaderBumped         )
  {
    super( fOrbitalSpeed,
           fOrbitalRotation,
           fRadius,
           fSatelliteDistance,
           fYAxisRotation,
           fYAxisRotationalSpeed,
           colorSurface           );
           
    m_imgTexture = imgTexture;
    m_imgNormalMap = imgNormalMap;
    m_shaderBumped = shaderBumped;
  }
  
  protected void Draw_Render()
  {
    
    shader( m_shaderBumped );

    fill( this.m_colorSurface.Red(),
          this.m_colorSurface.Green(),
          this.m_colorSurface.Blue()   );
          
    m_shaderBumped.set( "v4Color",
                        (this.m_colorSurface.Red()   / 256.0),
                        (this.m_colorSurface.Green() / 256.0),
                        (this.m_colorSurface.Blue()  / 256.0),
                        1.0                                  );
    
    
    print( "\nthis.m_colorSurface.Red()"+ this.m_colorSurface.Red()/ 256);
    print( "\nthis.m_colorSurface.Green()"+ this.m_colorSurface.Green()/ 256);
    print( "\nthis.m_colorSurface.Blue()"+ this.m_colorSurface.Blue()/ 256);
    /*
    m_shapeSphere.fill(this.m_colorSurface.Red(),
                       this.m_colorSurface.Green(),
                       this.m_colorSurface.Blue()   );*/
    shape( m_shapeSphere );
    
  }
  
  protected void Draw_RotateTranslate()
  {
    /*todo: bump !*/
    
    m_shapeSphere.setTexture( m_imgTexture );
    //m_shapeSphere.
    super.Draw_RotateTranslate();
    
  }
protected void Draw_After()
  {
    super.Draw_After();
    
  }
  
  public void Draw()
  {
    Draw_RotateTranslate();
    Draw_Render();
    Draw_After();
  }
  
  public void Setup()
  {
    m_shapeSphere = createShape( SPHERE, m_fRadius );
    //m_shapeSphere.noStroke();
    m_shapeSphere.setTexture( m_imgTexture );
    m_shapeSphere.fill(  this.m_colorSurface.Red(),
                         this.m_colorSurface.Green(),
                         this.m_colorSurface.Blue()   );
    
  }  
}

public class CCelestialBodyTextured extends CCelestialBody
{
  public PImage  m_imgTexture;  
  public PShader m_shaderTextured;
  public PShape  m_shapeSphere;
  
  CCelestialBodyTextured( float   fOrbitalSpeed,
                          float   fOrbitalRotation,
                          float   fRadius,
                          float   fSatelliteDistance,
                          float   fYAxisRotation,
                          float   fYAxisRotationalSpeed,
                          CColor  colorSurface,
                          PImage  imgTexture,
                          PShader shaderTextured         )
  {
    super( fOrbitalSpeed,
           fOrbitalRotation,
           fRadius,
           fSatelliteDistance,
           fYAxisRotation,
           fYAxisRotationalSpeed,
           colorSurface           );
           
    m_imgTexture = imgTexture;
    m_shaderTextured = shaderTextured;
  }

  protected void Draw_Render()
  {
    shader( m_shaderTextured );
    noLights();
    m_shaderTextured.set( "v4Color",
                          (this.m_colorSurface.Red()   / 256.0),
                          (this.m_colorSurface.Green() / 256.0),
                          (this.m_colorSurface.Blue()  / 256.0),
                          (1.0                                )  );
    /*                      
    fill(  this.m_colorSurface.Red(),
           this.m_colorSurface.Green(),
           this.m_colorSurface.Blue()   );
    */       
    shape( m_shapeSphere );
    pointLight( 255, 255, 255, 0, 0, 0 );
  }
  
  protected void Draw_RotateTranslate()
  {
    
    m_shapeSphere.setTexture( m_imgTexture );
    super.Draw_RotateTranslate();
    
  }
  
  protected void Draw_After()
  {
    super.Draw_After();
    
  }
  
  public void Draw()
  {
    Draw_RotateTranslate();
    Draw_Render();
    Draw_After();
  }
  
  public void Setup()
  {
    m_shapeSphere = createShape( SPHERE, m_fRadius );
    //m_shapeSphere.noStroke();
    m_shapeSphere.setTexture( m_imgTexture );
    
  }
  
}
