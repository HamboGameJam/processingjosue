import processing.opengl.*;
import toxi.geom.*;
import toxi.geom.mesh.*;
import toxi.geom.mesh.STLReader;
import toxi.geom.mesh.TriangleMesh;
import toxi.processing.*;

String[] arrStrSTLFileNames = { "tetrahedron.stl",
                                "truncated-tetrahedron.stl" };
                                
String[] arrStrSTLFiles = arrStrSTLFileNames;

ArrayList<TriangleMesh> arrTriangleMeshObjs = new ArrayList<TriangleMesh>();

ToxiclibsSupport objToxiclibsSupport;

int max_size = 80;
int min_size = 20;
int num_steps = 4;

void setup()
{
  size( 640, 480, OPENGL );
  
  STLReader objSTLReader = new STLReader();
  TriangleMesh objTriangleMesh;
  
  for(String strSTLFile : arrStrSTLFiles)
  {
     objTriangleMesh =
      (TriangleMesh) new STLReader().loadBinary( sketchPath("data\\"+strSTLFile),
                                                 STLReader.TRIANGLEMESH           );
      
    arrTriangleMeshObjs.add(objTriangleMesh); 
    
    //println("data/" + strSTLFile);
    //objSTLReader
    //arrMesh3Ds
    
    
    //STL objSTL = new STL(this, "data/" + strSTLFile);
    /*FaceList objFaceList = objSTL.getPolyData();
    objFaceList.normalize( max_size );
    objFaceList.center();
    arrFaceLists.add( objFaceList );
    */
  }
  
  objToxiclibsSupport = new ToxiclibsSupport(this);
   
}

void draw()
{
  noStroke();
  background( 255 );
  lights();
  
  float x = cos( radians( frameCount ) ) * 1000;
  float z = sin( radians( frameCount ) ) * 1000;
  camera( x, 0, z, width/2,  height/2, 0, 0, 1, 0 );
  
  pushMatrix();  
    translate(width/2, height/2, 0);
    
    TriangleMesh objTriangleMesh =
      arrTriangleMeshObjs.get(1);
      
    //println("objTriangleMesh==null " + (objTriangleMesh==null) );  
    objToxiclibsSupport.mesh( objTriangleMesh );
    
  popMatrix();
  
  /*
  pushMatrix();
    translate( width/2, height/2, 0 );
    rotateY( radians( frameCount ) );
    */
    /*
    FaceList objFaceList = arrFaceLists.get(0);
    objFaceList.draw( this );
    */
    /*
    pushMatrix();
      rotateZ( radians( frameCount ) );
      fill( 255, 0, 0 );
      beginShape( TRIANGLE_STRIP );
        for ( int i = 0; i < 20; i++ )
        {
          float x1 = cos( radians( i * 10 ) ) * 100;
          float y1 = sin( radians( i * 10 ) ) * 100;
          float x2 = cos( radians( i * 10 + 5 ) ) * ( 180 - i * 4 );
          float y2 = sin( radians( i * 10 + 5 ) ) * ( 180 - i * 4 );
          vertex( x1, y1, 0 );
          vertex( x2, y2, 50 + i );
        }
      endShape();
    popMatrix();
    pushMatrix();
      translate( 0, 0, -100 );
      rotateZ( radians( -frameCount ) );
      fill( 255, 255, 0 );
      beginShape( QUAD_STRIP );
        for ( int i = 0; i < 20; i++ )
        {
          float x1 = cos( radians( i * 10 ) ) * ( 100 + i * 5 );
          float y1 = sin( radians( i * 10 ) ) * ( 100 + i * 5 );
          float x2 = cos( radians( i * 10 + 5 ) ) * 180;
          float y2 = sin( radians( i * 10 + 5 ) ) * 180;
          vertex( x1, y1, 0 );
          vertex( x2, y2, 80 + i * 2 );
        }
      endShape();
    popMatrix();
    */
  //popMatrix();    
}

