#define PROCESSING_TEXTURE_SHADER

struct tDataFrag
{
	vec3 v3Vertex;
	vec3 v3Normal;
};

attribute vec2 texCoord;

uniform mat3 normalMatrix;
uniform mat4 texMatrix;

varying tDataFrag dataFrag;
varying vec4 v4TexCoord;

void main()
{
	dataFrag.v3Vertex = gl_Vertex.xyz;
	dataFrag.v3Normal = normalize( normalMatrix * gl_Normal ).xyz;
	
	v4TexCoord = texMatrix * vec4( texCoord, 1.0, 1.0 );
	
	gl_Position = gl_ModelViewMatrix * gl_Vertex;
}