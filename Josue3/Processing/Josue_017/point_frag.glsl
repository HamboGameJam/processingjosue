#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

struct tDataFrag
{
	vec3 v3Vertex;
	vec3 v3Normal;
};

uniform sampler2D texture;
uniform vec4 v4LightColor;
uniform vec4 v4LightPosition;

varying vec4 vertTexCoord;
varying tDataFrag dataFrag;

void main()
{
	//calculate the location of this fragment (pixel) in world coordinates
	vec3 v3FragPosition = vec3(  gl_ModelViewMatrix * vec4( dataFrag.v3Vertex, 1 )  );
	
	
	//calculate the vector from this pixels surface to the light source
	vec3 v3SurfaceToLight = v4LightPosition.xyz - v3FragPosition;
	
	
	//calculate the cosine of the angle of incidence (brightness)
	float fBrightness = (  dot( dataFrag.v3Normal, v3SurfaceToLight )                )/
						(  length( v3SurfaceToLight ) * length( dataFrag.v3Normal )  );
	
	fBrightness = clamp( fBrightness, 0.00, 1.00 );
	

    //calculate final color of the pixel, based on:
    // 1. The angle of incidence: fBrightness
    // 2. The color of the light: v4LightColor
    // 3. The texture and texture coord: texture(tex, fragTexCoord)	
	
	//gl_FragColor = vec4( fBrightness, fBrightness, fBrightness, 1.00  );
	
	vec4 v4FragColor  = (  fBrightness *
						   v4LightColor  );/*  *
						   texture2D( texture, vertTexCoord.st )  ); */
	
	gl_FragColor = vec4( fBrightness, fBrightness, fBrightness, 1.00  );
	
	/*
	gl_FragColor = (  fBrightness *
					  vec4( v4LightColor ) *
					  texture2D( texture, vertTexCoord.st )  );
	*/
	
}