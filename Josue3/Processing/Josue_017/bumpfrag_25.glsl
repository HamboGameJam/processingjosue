#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

struct tData
{
	vec3 v3Normal;
	vec3 v3EyeVector;
	vec3 v3LightDirection;	
};

uniform sampler2D texture;
uniform float fShininess;
uniform vec4 v4MaterialAmbient;
uniform vec4 v4MaterialDiffuse;
uniform vec4 v4MaterialSpecular;

varying vec4 vertColor;
varying vec4 vertTexCoord;
varying tData dataOut;

void main()
{
  //vec4 v4Specular = vec4(0.0);
  
  float fSpecular;
  
  vec3 v3NormalizedNormal =
	normalize( dataOut.v3Normal );
	
  vec3 v3NormalizedEyeVector = 
	normalize( dataOut.v3EyeVector );
	
  vec3 v3NormalizedLightDirection =
	normalize( dataOut.v3LightDirection );
	

  float fLambertIntensity = max(  dot( v3NormalizedNormal,	
							           v3NormalizedLightDirection ),
								  0.00                               );
  if( fLambertIntensity > 0.00 )
  {
    /*
	vec3 v3H =
  	  normalize( v3NormalizedLightDirection + v3NormalizedEyeVector );
	  
	float fIntSpecular = max(  dot( v3H, v3NormalizedNormal ), 0.0  );
	
	v4Specular = v4MaterialSpecular * pow( fIntSpecular, fShininess );
	*/
	vec3 v3Reflexion = reflect( -v3NormalizedLightDirection,
	                             v3NormalizedNormal          );
								 
	fSpecular = pow( 
					  (  max( dot( v3Reflexion, v3NormalizedNormal),
						      0.0                                    )  ),
					  (  fShininess                                     )	);
								 
  }
								
  gl_FragColor = max( (texture2D(texture, vertTexCoord.st) + v4MaterialAmbient + (v4MaterialDiffuse * fSpecular))* fLambertIntensity ,
				       texture2D(texture, vertTexCoord.st) * v4MaterialAmbient);
}