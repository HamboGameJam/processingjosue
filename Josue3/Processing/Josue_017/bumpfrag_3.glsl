#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

struct tData
{
	vec3 v3Normal;
	vec4 v4Position;
	vec4 v4TexCoord;	
};

//uniform vec4  
uniform vec4  v4MaterialDiffuse;
uniform vec4  v4MaterialSpecular;
uniform float fMaterialShininnes;

uniform vec4  v4LightPosition;
uniform vec4  v4LightSpecular;
uniform vec4  v4LightDiffuse;


varying tData data;

void main()
{
	vec4 v4EyePosition = -normalize( data.v4Position - v4LightPosition );
	vec3 v3Light = v4EyePosition.xyz;
	vec3 v3Reflexion = normalize(  -reflect( v3Light, v4EyePosition )  );
	vec3 v3VertexPosition = normalize(  -data.v4Position.xyz;  );
	
	vec4 v4DiffuseComponent =
		(  v4MaterialDiffuse                                   )*
		(  max( 0.0, dot( data.v3normal, v4EyePosition.xyz) )  )*
		(  v4LightDiffuse                                      );
	
	vec4 v4SpecularComponent;
	
	if( fMaterialShininnes == 0.00 )
	{
		v4SpecularComponent =
			vec4( 0.0, 0.0, 0.0, 0.0 );
	}
	else
	{
		v4SpecularComponent =
			v4LightSpecular *
			v4MaterialSpecular *
			pow(  ( max( 0.0,( dot( v3Reflexion, v3VertexPosition ) ) ),
			      ( fMaterialShininnes                                )  );
	}
		
	gl_FragColor = v4DiffuseComponent +
				   v4SpecularComponent;
	
}