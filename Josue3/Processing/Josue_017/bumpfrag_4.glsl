#ifdef GL_ES
	precision mediump float;
	precision mediump int;
#endif

varying vec3 v3LightDirection;
varying vec3 v3EyeDirection;
varying vec4 v4TexCoord0;

uniform vec4  v4MaterialDiffuse;
uniform float fBumpDensity;
uniform float fBumpSize;
uniform float fSpecularFactor;

void main()
{
	vec3 v3LitColor;
	vec2 v2BumpPosition = fBumpDensity * v4TexCoord0.st;
	vec2 v2Perturbation = fract(v2BumpPosition) - vec2(0.5);
	float fPseudoDistance =
		v2Perturbation.x * v2Perturbation.x +
		v2Perturbation.y * v2Perturbation.y;
		
	float fNormalizationFactor =
		1.0 / sqrt( fPseudoDistance + 1.0 );
	
	if( fPseudoDistance >= fBumpSize )
	{
		v2Perturbation = vec2(0.0);
		fNormalizationFactor = 1.0;
	}
	
	vec3 v3NormalDelta =
		vec3( fPseudoDistance.x,
			  fPseudoDistance.y,
			  1.0                ) *
		fNormalizationFactor;
		
	v3LitColor = v4MaterialDiffuse *
	             max( dot( v3NormalDelta, v3LightDirection ), 0.0 );
				 
	vec3 v3ReflexionDirection = 
		reflect( v3LightDirection, v3NormalDelta );
	
	float fSpecularComponent =
		max( dot( v3EyeDirection, v3ReflexionDirection), 0.0 );
	fSpecularComponent = pow( fSpecularComponent, 6.0 );
	fSpecularComponent *= fSpecularFactor;
	
	v3LitColor = min( litcolot + fSpecularComponent, vec3(1.0) );
	
	gl_FragColor = vec3( v3LitColor, 1.0 );
	
}