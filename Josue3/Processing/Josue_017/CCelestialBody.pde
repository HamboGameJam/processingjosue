public class CCelestialBody //<>//
{
  public float     m_fOrbitalSpeed;
  public float     m_fOrbitalRotation;
  public float     m_fRadius;
  public float     m_fSatelliteDistance;
  public float     m_fYAxisRotation;
  public float     m_fYAxisRotationalSpeed;
  public CColor    m_colorSurface;
  public ArrayList<CCelestialBody> m_arrayListSatellites;
  //tilt
  
  CCelestialBody( float     fOrbitalSpeed,
                  float     fOrbitalRotation,
                  float     fRadius,
                  float     fSatelliteDistance,
                  float     fYAxisRotation,
                  float     fYAxisRotationalSpeed,
                  CColor    colorSurface           )
  {
    this.m_fOrbitalSpeed = fOrbitalSpeed;
    this.m_fOrbitalRotation = fOrbitalRotation;
    this.m_fRadius = fRadius;
    this.m_fSatelliteDistance = fSatelliteDistance;
    this.m_fYAxisRotation = fYAxisRotation;    
    this.m_fYAxisRotationalSpeed = fYAxisRotationalSpeed;
    this.m_colorSurface = colorSurface;
    this.m_arrayListSatellites = new ArrayList<CCelestialBody>();
    
    
    
  }
  
  public void Print()
  {
    print( "\nOrbital Speed:"      + this.m_fOrbitalSpeed );
    print( "\nOrbital Rotation:"   + this.m_fOrbitalRotation );
    print( "\nRadius:"             + this.m_fRadius );
    print( "\nSatellite Distance:" + this.m_fSatelliteDistance );
    print( "\nY Axis Rotation:"    + this.m_fYAxisRotation );    
    print( "\nY Axis Rotational Speed:"    + this.m_fYAxisRotationalSpeed );   
    //this.m_colorSurface.Print();
  }
  
  public void Setup()
  {

  }
  
  public void SatelliteAdd( CCelestialBody celestialBodySatellite )
  {
    m_arrayListSatellites.add( celestialBodySatellite );
  }
  
  public void SatelliteRemove( CCelestialBody celestialBodySatellite )
  {
    m_arrayListSatellites.remove( celestialBodySatellite );
  }
  
  protected void Draw_Render()
  {
    fill(  this.m_colorSurface.Red(),
           this.m_colorSurface.Green(),
           this.m_colorSurface.Blue()   );
        
    sphere(m_fRadius);  
  }
  
  protected void Draw_RotateTranslate()
  {
    this.m_fOrbitalRotation = ( this.m_fOrbitalSpeed * frameCount );
    this.m_fYAxisRotation   = ( this.m_fYAxisRotationalSpeed * frameCount );

    pushMatrix();
      rotateY( this.m_fOrbitalRotation );
      translate( 0, 0, this.m_fSatelliteDistance );
      rotateY(-this.m_fOrbitalRotation );
      pushMatrix();        
        rotateY( this.m_fYAxisRotation );
  }
  
  protected void Draw_After()
  {
    /**/
        for( int iSatelliteIndex = 0;
                 iSatelliteIndex < this.m_arrayListSatellites.size();
                 iSatelliteIndex ++                                   )
        {
          ( this.m_arrayListSatellites.get(iSatelliteIndex) ).Draw();
          
        }        

      popMatrix();
      
      
    popMatrix();
  }
  
  public void Draw( )
  {
    //shader(null);
    Draw_RotateTranslate();
    Draw_Render();
    Draw_After();
  }

}
public class CCelestialBodyBumped extends CCelestialBody
{
  public PImage  m_imgTexture;
  public PImage  m_imgNormalMap; 
  public PShader m_shaderBumped;
  public PShape  m_shapeSphere;
  public float[] m_arrf4MaterialAmbient;
  public float[] m_arrf4MaterialDiffuse;
  public float[] m_arrf4MaterialSpecular;
  public float   m_fMaterialShininess;
  
  CCelestialBodyBumped( float   fOrbitalSpeed,
                        float   fOrbitalRotation,
                        float   fRadius,
                        float   fSatelliteDistance,
                        float   fYAxisRotation,
                        float   fYAxisRotationalSpeed,
                        CColor  colorSurface,
                        PImage  imgTexture,
                        PImage  imgNormalMap,
                        float[] arrf4MaterialAmbient,
                        float[] arrf4MaterialDiffuse,
                        float[] arrf4MaterialSpecular,
                        float   fMaterialShininess,
                        PShader shaderBumped          )
  {
    super( fOrbitalSpeed,
           fOrbitalRotation,
           fRadius,
           fSatelliteDistance,
           fYAxisRotation,
           fYAxisRotationalSpeed,
           colorSurface           );
           
    m_imgTexture = imgTexture;
    m_imgNormalMap = imgNormalMap;
    m_arrf4MaterialAmbient = arrf4MaterialAmbient;
    m_arrf4MaterialDiffuse = arrf4MaterialDiffuse;
    m_arrf4MaterialSpecular = arrf4MaterialSpecular;
    m_fMaterialShininess = fMaterialShininess;
    
    m_shaderBumped = shaderBumped;
    
  }
  
  protected void Draw_Render()
  {
    shader( m_shaderBumped );
    
    m_shaderBumped.set( ("v4LightPosition"                  ),
                        (0.00                               ),
                        (0.00                               ),
                        (0.00                               ),
                        (1.0                                )  );
                        
    m_shaderBumped.set( ("v4Color"                          ),
                        (this.m_colorSurface.Red()   / 256.0),
                        (this.m_colorSurface.Green() / 256.0),
                        (this.m_colorSurface.Blue()  / 256.0),
                        (1.0                                )  );
                        
    m_shaderBumped.set( ("v4MaterialAmbient"                ),
                        (m_arrf4MaterialAmbient[0]          ),
                        (m_arrf4MaterialAmbient[1]          ),
                        (m_arrf4MaterialAmbient[2]          ),
                        (m_arrf4MaterialAmbient[3]          )  );
                        
    m_shaderBumped.set( ("v4MaterialDiffuse"                ),
                        (m_arrf4MaterialDiffuse[0]          ),
                        (m_arrf4MaterialDiffuse[1]          ),
                        (m_arrf4MaterialDiffuse[2]          ),
                        (m_arrf4MaterialDiffuse[3]          )  );
                        
    m_shaderBumped.set( ("v4MaterialSpecular"               ),
                        (m_arrf4MaterialSpecular[0]         ),
                        (m_arrf4MaterialSpecular[1]         ),
                        (m_arrf4MaterialSpecular[2]         ),
                        (m_arrf4MaterialSpecular[3]         )  );
                        
    m_shaderBumped.set( "fMaterialShininnes", m_fMaterialShininess );
    
    shape( m_shapeSphere );
    
  }
  
  protected void Draw_RotateTranslate()
  { 
    m_shapeSphere.setTexture( m_imgTexture );
    super.Draw_RotateTranslate();
    
  }
protected void Draw_After()
  {
    super.Draw_After();
    
  }
  
  public void Draw()
  {
    Draw_RotateTranslate();
    Draw_Render();
    Draw_After();
  }
  
  public void Setup()
  {
    m_shapeSphere = createShape( SPHERE, m_fRadius );
    m_shapeSphere.setTexture( m_imgTexture );

    
  }  
}

public class CCelestialBodyTextured extends CCelestialBody
{
  public PImage  m_imgTexture;  
  public PShader m_shaderTextured;
  public PShape  m_shapeSphere;
  
  CCelestialBodyTextured( float   fOrbitalSpeed,
                          float   fOrbitalRotation,
                          float   fRadius,
                          float   fSatelliteDistance,
                          float   fYAxisRotation,
                          float   fYAxisRotationalSpeed,
                          CColor  colorSurface,
                          PImage  imgTexture,
                          PShader shaderTextured         )
  {
    super( fOrbitalSpeed,
           fOrbitalRotation,
           fRadius,
           fSatelliteDistance,
           fYAxisRotation,
           fYAxisRotationalSpeed,
           colorSurface           );
           
    m_imgTexture = imgTexture;
    m_shaderTextured = shaderTextured;
  }

  protected void Draw_Render()
  {
    shader( m_shaderTextured );

    m_shaderTextured.set( ("v4Color"                          ),
                          (this.m_colorSurface.Red()   / 256.0),
                          (this.m_colorSurface.Green() / 256.0),
                          (this.m_colorSurface.Blue()  / 256.0),
                          (1.0                                )  );
     
    shape( m_shapeSphere );

  }
  
  protected void Draw_RotateTranslate()
  {
    
    m_shapeSphere.setTexture( m_imgTexture );
    super.Draw_RotateTranslate();
    
  }
  
  protected void Draw_After()
  {
    super.Draw_After();
    
  }
  
  public void Draw()
  {
    Draw_RotateTranslate();
    Draw_Render();
    Draw_After();
    
  }
  
  public void Setup()
  {
    m_shapeSphere = createShape( SPHERE, m_fRadius );
    m_shapeSphere.setTexture( m_imgTexture );
    
  }
  
}
