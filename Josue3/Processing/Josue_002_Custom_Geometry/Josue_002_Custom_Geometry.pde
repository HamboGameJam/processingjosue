import processing.opengl.*;


int lightMode;
int lightDirection;

void setup()
{
  size( 640, 480, OPENGL );
  
  lightMode = 0;
  lightDirection = 0;
    
}

void cylinder( int numSegments, float h, float r )
{
  float angle = 360.0 / (float)numSegments;
  
  // top
  beginShape();
  for ( int i = 0; i < numSegments; i++ )
  {
    float x = cos( radians( angle * i ) ) * r;
    float y = sin( radians( angle * i ) ) * r;
    vertex( x, y, -h/2 );
  }
  endShape( CLOSE );
  
  // side
  beginShape( QUAD_STRIP );
  for ( int i = 0; i < numSegments + 1; i++ )
  {
    float x = cos( radians( angle * i ) ) * r;
    float y = sin( radians( angle * i ) ) * r;
    vertex( x, y, -h/2 );
    vertex( x, y, h/2 );
  }
  endShape();
  
  // bottom
  beginShape();
  for ( int i = 0; i < numSegments; i++ )
  {
    float x = cos( radians( angle * i ) ) * r;
    float y = sin( radians( angle * i ) ) * r;
    vertex( x, y, h/2 );
  }
  endShape( CLOSE );
}

void keyPressed()
{
  switch ( key )
  {
    case 'n':
      lightMode = 0; // no lights
      break;
  
    case 'l':
      lightMode = 1; // lights
      break;
      
    case 'd':
      lightMode = 2; // directional light
      break;
      
    case 'a':
      lightMode = 3; // ambient light
      break;
      
    case 'p':
      lightMode = 4; // point light
      break;
      
    case 's':
      lightMode = 5; // spot light
      break;
  }
  
  if ( key == CODED )
  {
    switch ( keyCode )
    {
      case UP:
        lightDirection = 0;
        break;
        
      case RIGHT:
        lightDirection = 1;
        break;
        
      case DOWN:
        lightDirection = 2;
        break;
        
      case LEFT:
        lightDirection = 3;
        break;
    }
  }
  
}

void draw()
{
  background( 0 );
  
  lights();
  
  switch ( lightMode )
  {
    case 0:
      noLights();
      break;
    case 1:
      lights();
      break;    
    case 2:
      if( lightDirection == 0 )
      {
        directionalLight( 255, 128, 0, 0, -1, 0 ); // UP
      }
      else if( lightDirection == 1 )
      {
        directionalLight( 0, 255, 0, 1, 0, 0 ); // RIGHT
      }
      else if( lightDirection == 2 )
      {
        directionalLight( 255, 0, 255, 0, 1, 0 ); // DOWN
      }
      else if ( lightDirection == 3 )
      {
        directionalLight( 0, 255, 255, -1, 0, 0 ); // LEFT
      }
      break;
      
    case 3:
      ambientLight( 0, 255, 255 );
      break;
      
    case 4:
      pointLight( 255, 255, 0, 100, height*0.3, 100 );
      break;
      
    case 5:
      spotLight( 128, 255, 128, 800, 20, 300, -1, .25, 0, PI, 2 );
      break;
      
    default:
      noLights();
  }        
  
  /*
  pushMatrix();
    translate( width/2, height/2, 0 );
    pushMatrix();
      rotateY( radians( frameCount ) );
      fill( 255 );
      noStroke();
      sphere( 100 );
    popMatrix();
    pushMatrix();
      rotateZ( radians( frameCount ) );
      rotateX( radians( frameCount/2 ) );
      fill( 255 );
      noStroke();
      box( 150 );
    popMatrix();
  popMatrix();
*/

  pushMatrix();
    translate( width*.3, height*.3, 0 );
    rotateY( radians( frameCount ) );
    fill( 255, 0, 0 );
    cylinder( 30, 100, 50 );
  popMatrix();
  
  pushMatrix();
    translate( width*.7, height*.5, 0 );
    rotateY( radians( frameCount ) );
    fill( 255, 255, 0 );
    cylinder( 4, 200, 50 );
  popMatrix();
  
  pushMatrix();
    translate( width*.3, height*.7, 0 );
    rotateY( radians( frameCount ) );
    fill( 0, 0, 255 );
    cylinder( 3, 200, 30 );
  popMatrix();
/*
  float angleY = radians( frameCount );
  
  pushMatrix();
    translate( width * 0.3, height* 0.3 );
    rotateY( angleY );
    fill( 0, 255, 255 );
    box( 100 );
  popMatrix();
   
  pushMatrix();
    translate( width * 0.5, height* 0.5 );
    rotateY( angleY );
    fill( 0, 255, 0 );
    box( 100, 40, 50 );
  popMatrix();
  
  pushMatrix();
    translate( width * 0.7, height * 0.3 );
    rotateY( angleY );
    fill( 255, 0, 0 );
    sphereDetail( 30 );
    sphere( 75 );
  popMatrix();
  
  pushMatrix();
    translate( width * 0.3, height * 0.7 );
    rotateY( angleY );
    fill( 255, 255, 0 );
    sphereDetail( 6 );
    sphere( 75 );
  popMatrix();
  
  pushMatrix();
    translate( width * 0.7, height * 0.7 );
    rotateY( angleY );
    fill( 255, 0, 255 );
    sphereDetail( 4, 20 );
    sphere( 75 );
  popMatrix();
 */ 
}

