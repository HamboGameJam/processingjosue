#define PROCESSING_TEXTURE_SHADER

struct tData
{
	vec3 v3Normal;
	vec4 v3Position;
	//vec4 v3RawPosition;	
};

uniform mat4 transform;
uniform mat4 texMatrix;
uniform mat3 normalMatrix;

attribute vec3 normal;
attribute vec2 texCoord;

varying tData data;

void main()
{
	data.v3Normal = normalMatrix * normal;
	data.v3Position = transform * vertex;
	//data.v3RawPosition = vertex;
	
	vertTexCoord = texMatrix * vec4( texCoord, 1.0, 1.0 );
	
	gl_Position = transform * vertex;
}
