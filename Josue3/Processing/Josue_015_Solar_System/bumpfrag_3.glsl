#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

struct tData
{
	vec3 v3Normal;
	vec4 v3Position;
	vec4 v3RawPosition;	
};

varying tData data;