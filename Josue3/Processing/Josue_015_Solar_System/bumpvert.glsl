#define PROCESSING_TEXTURE_SHADER

struct tData
{
	vec3 v3Normal;
	vec3 v3EyePosition;
	vec3 v3LightDirection;	
};

uniform mat4 transform;
uniform mat4 texMatrix;
uniform mat3 normalMatrix;
uniform float fShininess;
uniform vec4 v4Color;
uniform vec4 v4LightPosition;

uniform vec4 v4MaterialAmbient;
uniform vec4 v4MaterialDiffuse;
uniform vec4 v4MaterialSpecular;


attribute vec4 vertex;
attribute vec3 normal;
attribute vec2 texCoord;

varying vec4 vertColor;
varying vec4 vertTexCoord;
varying tData dataOut;

void main()
{ 
  vec4 v4PositionTransformed = transform * vertex;
  
  dataOut.v3Normal = normalize( normalMatrix * normal );
  dataOut.v3LightDirection = vec3( v4LightPosition - v4PositionTransformed );
  dataOut.v3EyePosition = vec3( -v4PositionTransformed );
  
  vertColor = v4Color;
  vertTexCoord = texMatrix * vec4( texCoord, 1.0, 1.0 );
  
  gl_Position = transform * vertex;
}