
<!-- saved from url=(0127)https://raw.githubusercontent.com/martymcguire/ThingiverseCollage/master/libraries/unlekkerLib/src/unlekker/util/ColorUtil.java -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">package unlekker.util;

import java.awt.Color;

public class ColorUtil {
  public static float hsb[];

  public ColorUtil() {
  }

  public static final int colorBlended(float fract,
                       float r, float g, float b,
                       float r2, float g2, float b2) {

    r2 = (r2 - r);
    g2 = (g2 - g);
    b2 = (b2 - b);
    return color(r + r2 * fract, g + g2 * fract, b + b2 * fract);
  }

  public static final int colorAdjust(int c,float bright,float saturation) {
    if(hsb==null) hsb=new float[3];
    Color.RGBtoHSB((c &gt;&gt; 16)&amp;0xff, (c &gt;&gt; 8)&amp;0xff, (c &amp;0xff), hsb);

    hsb[1]=saturation;
    hsb[2]=bright;
    c=Color.HSBtoRGB(hsb[0], hsb[1], hsb[2]);
    return c;
  }

  public static final int colorMult(int c,float m) {
    int cr,cg,cb;
    cr=(int)((float)((c &gt;&gt; 16)&amp;0xff)*m);
    cg=(int)((float)((c &gt;&gt; 8)&amp;0xff)*m);
    cb=(int)((float)((c&amp;0xff))*m);
    if(cr&gt;255) cr=255;
    if(cg&gt;255) cg=255;
    if(cb&gt;255) cb=255;
    return color(cr,cg,cb);
  }

  public static final int colorBlended(float fract,
                       float r, float g, float b, float a,
                       float r2, float g2, float b2, float a2) {


    r2 = (r2 - r);
    g2 = (g2 - g);
    b2 = (b2 - b);
    a2 = a2 - a;
    return color(r + r2 * fract, g + g2 * fract, b + b2 * fract, a + a2 * fract);
  }

  public static final int colorBlended(float fract, String c1,String c2) {
    int col1=color(c1);
    int col2=color(c2);
    return colorBlended(fract, (col1  &gt;&gt; 16)&amp;0xff,(col1  &gt;&gt; 8)&amp;0xff,(col1)&amp;0xff,
                        (col2  &gt;&gt; 16)&amp;0xff,(col2  &gt;&gt; 8)&amp;0xff,(col2)&amp;0xff);
  }

  public static final int color(String hex) {
   return 0xff000000 | Integer.parseInt(hex, 16);
  }

  public static final String colorToString(int c) {
    return("rgba=("+((c &gt;&gt; 16)&amp;0xff)+","+((c &gt;&gt; 8)&amp;0xff)+","+(c&amp;0xff)+","+((c &gt;&gt; 24)&amp;0xff)+")");
  }

  public static String colorToHex(int col) {
    String s=Integer.toHexString((col &gt;&gt; 16)&amp;0xff)+
        Integer.toHexString((col &gt;&gt; 8)&amp;0xff)+
        Integer.toHexString((col)&amp;0xff);
    s=s.toUpperCase();
    return s;
  }

  public static String colorToHex(int r,int g,int b) {
    return colorToHex(color(r,g,b));
  }

  public static final int color(int x, int y, int z, int a) {
    return ( (a &amp; 0xff) &lt;&lt; 24) |
        ( (x &amp; 0xff) &lt;&lt; 16) | ( (y &amp; 0xff) &lt;&lt; 8) | (z &amp; 0xff);
  }

  public static final int color(float x, float y, float z, float a) {
    return ( ( (int) a &amp; 0xff) &lt;&lt; 24) |
        ( ( (int) x &amp; 0xff) &lt;&lt; 16) | ( ( (int) y &amp; 0xff) &lt;&lt; 8) |
        ( (int) z &amp; 0xff);
  }

  public static final int color(int x, int y, int z) {
    return 0xff000000 |
        ( (x &amp; 0xff) &lt;&lt; 16) | ( (y &amp; 0xff) &lt;&lt; 8) | (z &amp; 0xff);
  }

  public static final int color(float x, float y, float z) {
    return 0xff000000 |
        ( ( (int) x &amp; 0xff) &lt;&lt; 16) | ( ( (int) y &amp; 0xff) &lt;&lt; 8) |
        ( (int) z &amp; 0xff);
  }

  public static final int getAlpha(int c) {
    return (c &gt;&gt; 24)&amp;0xff;
  }

  public static final int setAlpha(int c, float alpha) {
    return ( ( (int) alpha &lt;&lt; 24) | (0x00ffffff &amp; c));
  }

  public static final float getBrightness(int c) {
    if(hsb==null) hsb=new float[3];
    Color.RGBtoHSB((c &gt;&gt; 16)&amp;0xff, (c &gt;&gt; 8)&amp;0xff, (c &amp;0xff), hsb);

    return hsb[2];
  }

  public static final float getSaturation(int c) {
    if(hsb==null) hsb=new float[3];
    Color.RGBtoHSB((c &gt;&gt; 16)&amp;0xff, (c &gt;&gt; 8)&amp;0xff, (c &amp;0xff), hsb);

    return hsb[1];
  }


  public boolean isWhite(int c) {
    return ((c &amp; 0xffffff)==0xffffff);
  }
}
</pre></body></html>