import processing.opengl.*;

PImage texture1;
PImage texture2;

void setup()
{
  size( 640, 480, OPENGL );
  
  noStroke();
  
  texture1 = loadImage("F1.PNG");
  texture2 = loadImage("F2.PNG");
}

void draw()
{
  background( 255 );
  
  textureMode( IMAGE );
  
  pushMatrix();
    translate( width/4, height/2, 0 );
    rotateY( radians( frameCount ) );
    beginShape();
      texture( texture1 );
      vertex( -100, -100, 0, 0 );
      vertex( 100, -100, 640, 0 );
      vertex( 100, 100, 640, 640 );
      vertex( -100, 100, 0, 640 );
    endShape( CLOSE );
  popMatrix();
  
  //textureMode( NORMALIZED );
  pushMatrix();
    translate( width*.75, height/2, 0 );
    rotateY( radians( -frameCount ) );
    beginShape();
      texture( texture2 );
      vertex( -100, -100, 0, 0 );
      vertex( 100, -100, 1, 0 );
      vertex( 100, 100, 1, 1 );
      vertex( -100, 100, 0, 1 );
    endShape( CLOSE );
  popMatrix();   
}

