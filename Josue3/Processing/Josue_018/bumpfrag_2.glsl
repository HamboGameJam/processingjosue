#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

struct tData
{
	vec3 v3Normal;
	vec3 v3EyePosition;
	vec3 v3LightDirection;	
};

uniform sampler2D texture;
uniform float fShininess;
uniform vec4 v4MaterialAmbient;
uniform vec4 v4MaterialDiffuse;
uniform vec4 v4MaterialSpecular;

varying vec4 vertColor;
varying vec4 vertTexCoord;
varying tData dataOut;

void main()
{
  vec4 v4Specular = vec4(0.0);
  
  vec3 v3NormalizedNormal =
	normalize( dataOut.v3Normal );
	
  vec3 v3NormalizedEyePosition = 
	normalize( dataOut.v3EyePosition );
	
  vec3 v3NormalizedLightDirection =
	normalize( dataOut.v3LightDirection );

  float fIntensity = max(  dot( v3NormalizedNormal,	
							    v3NormalizedLightDirection ), 0.00  );
  if( fIntensity > 0.00 )
  {
	vec3 v3H =
  	  normalize( v3NormalizedLightDirection + v3NormalizedEyePosition );
	  
	float fIntSpecular = max(  dot( v3H, v3NormalizedNormal ), 0.0  );
	
	v4Specular = v4MaterialSpecular * pow( fIntSpecular, fShininess );
  }
								
  gl_FragColor = //texture2D(texture, vertTexCoord.st) * vertColor *
                 //max( fIntensity * v4MaterialDiffuse + v4Specular, v4MaterialAmbient );
				 max( texture2D(texture, vertTexCoord.st) * (fIntensity * v4MaterialDiffuse) + v4Specular,
				      texture2D(texture, vertTexCoord.st) * v4MaterialAmbient );
}