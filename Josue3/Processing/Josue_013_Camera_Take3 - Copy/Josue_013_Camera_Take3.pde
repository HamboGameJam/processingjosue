//import peasy.*;
import processing.opengl.*;

//PeasyCam cam;

float fCameraEyePositionX = 0.00;
float fCameraEyePositionY = 0.00;
float fCameraEyePositionZ =-100.00;
float fCameraDestinationX = 0.00;
float fCameraDestinationY = 0.00;
float fCameraDestinationZ = 1.00;
float fCameraUpVectorX    = 0.00;
float fCameraUpVectorY    = 1.00;
float fCameraUpVectorZ    = 0.00;

void setup() {
  size(600,600,P3D);
  /*
  cam = new PeasyCam(this, 100);
  cam.setMinimumDistance(50);
  cam.setMaximumDistance(500);
  cam.setActive( false );
 */
}

void draw()
{
  //cam.lookAt(0,0,20);//
  
  //fCameraDestinationX += cos( radians( frameCount ) );
  //fCameraDestinationZ += sin( radians( frameCount ) );
  //x = 0;// cos( radians( frameCount ) ) * 1000;
  //z = -10;//sin( radians( frameCount ) ) * 1000;
  camera( fCameraEyePositionX,
          fCameraEyePositionY,
          fCameraEyePositionZ,
          fCameraDestinationX,
          fCameraDestinationY,
          fCameraDestinationZ,
          fCameraUpVectorX,
          fCameraUpVectorY,
          fCameraUpVectorZ     );
  
  background(0);
  
  pushMatrix();
    
    
    rotateX(-.5);
    rotateY(-.5);
    

    fill(255,0,0);
    box(100);
    //translate(0,0,2.5);
    
    pushMatrix();
      translate(0,0,5);
      fill(0,0,255);
      box(5);
    popMatrix();
    
  popMatrix();
  //cam.rotateZ( frameCount );
  //cam.feed();
  
}
